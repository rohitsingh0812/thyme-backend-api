BEGIN;
CREATE TABLE `google_account` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `email` varchar(100) NOT NULL,
    `password` varchar(100) NOT NULL,
    `token` varchar(100) NOT NULL
)
;
CREATE TABLE `google_calendar` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `account_id` integer NOT NULL,
    `uri` varchar(255) NOT NULL UNIQUE,
    `title` varchar(100) NOT NULL,
    `where` varchar(100) NOT NULL,
    `color` varchar(10) NOT NULL,
    `timezone` varchar(100) NOT NULL,
    `summary` longtext NOT NULL,
    `feed_uri` varchar(255) NOT NULL
)
;
ALTER TABLE `google_calendar` ADD CONSTRAINT `account_id_refs_id_47719f91` FOREIGN KEY (`account_id`) REFERENCES `google_account` (`id`);
CREATE TABLE `google_event` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `calendar_id` integer NOT NULL,
    `uri` varchar(255) NOT NULL UNIQUE,
    `title` varchar(255) NOT NULL,
    `edit_uri` varchar(255) NOT NULL,
    `view_uri` varchar(255) NOT NULL,
    `content` longtext NOT NULL,
    `start_time` datetime NOT NULL,
    `end_time` datetime NOT NULL
)
;
ALTER TABLE `google_event` ADD CONSTRAINT `calendar_id_refs_id_29549090` FOREIGN KEY (`calendar_id`) REFERENCES `google_calendar` (`id`);
CREATE INDEX `google_calendar_6f2fe10e` ON `google_calendar` (`account_id`);
CREATE INDEX `google_event_447205e2` ON `google_event` (`calendar_id`);
COMMIT;
