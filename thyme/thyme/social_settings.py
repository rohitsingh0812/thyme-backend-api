

GOOGLE_OAUTH_EXTRA_SCOPE = ['https://www.googleapis.com/auth/calendar']
GOOGLE_OAUTH2_AUTH_EXTRA_ARGUMENTS = {'access_type':'offline'}
GOOGLE_OAUTH2_CLIENT_ID           = '838690098622-fhvor7svh9enfhc17oh4ot9td02bjlts.apps.googleusercontent.com'
GOOGLE_OAUTH2_CLIENT_SECRET       = 'LPWXWdQvBUKzHNwQYdUhRVTF'
SOCIAL_AUTH_CREATE_USERS          = True
SOCIAL_AUTH_FORCE_RANDOM_USERNAME = False
SOCIAL_AUTH_DEFAULT_USERNAME      = 'socialauth_user'
SOCIAL_AUTH_COMPLETE_URL_NAME     = 'socialauth_complete'
LOGIN_ERROR_URL                   = '/auth/error/'
SOCIAL_AUTH_ERROR_KEY             = 'socialauth_error'

SOCIAL_AUTH_UUID_LENGTH = 10
SOCIAL_AUTH_SLUGIFY_USERNAMES = True
SOCIAL_AUTH_FORCE_POST_DISCONNECT = True
SOCIAL_AUTH_SESSION_EXPIRATION = False

LOGIN_URL          = '/be/auth/'
LOGIN_REDIRECT_URL = '/be/auth/done'
LOGIN_ERROR_URL    = '/be/auth/error/'

SOCIAL_AUTH_FIELDS_STORED_IN_SESSION = ['user',]
SOCIAL_AUTH_PIPELINE = (
    'social_auth.backends.pipeline.social.social_auth_user',
    'social_auth.backends.pipeline.associate.associate_by_email',
    'social_auth.backends.pipeline.misc.save_status_to_session',
    #'app.pipeline.redirect_to_form',
    #'app.pipeline.username',
    'social_auth.backends.pipeline.user.get_username',
    'social_auth.backends.pipeline.user.create_user',
    'social_auth.backends.pipeline.user.create_user',
    'social_auth.backends.pipeline.social.associate_user',
    'social_auth.backends.pipeline.social.load_extra_data',
    'social_auth.backends.pipeline.user.update_user_details',
    'backend.views.remember_user'
)

GOOGLE_API_CREDENTIAL_PATH = "/home/bitnami/storage/calendardata" #TODO Put this setting in db.py, very local!