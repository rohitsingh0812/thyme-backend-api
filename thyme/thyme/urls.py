from django.conf.urls import patterns, include, url
from rest_framework.urlpatterns import format_suffix_patterns
from backend import views
from django.contrib import admin
admin.autodiscover()
# from rest_framework.authtoken import views
# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    url(r'^get-login-token/', views.getToken.as_view()),#Login and get the Token
    url(r'^logout/', views.refreshToken.as_view()),#revoke the Token
    url(r'^get-user-info/', views.TokenUid.as_view()), #Get User Info after Auth
    url(r'^api-events/', views.getAllEventsFromApp.as_view()), #Get all events/invites for a particular user
    url(r'^api-events-all/', views.getAllEvents.as_view()),
    url(r'^api-event-info/', views.getEventDetails.as_view()), #Get info about a particular event
    #url(r'^cal-token/', views.storeCalToken.as_view()),
    url(r'^find-thyme/(\d{1})/$', views.findThyme.as_view()), #find feasible times
    url(r'^add-edit-event/', views.createOrEditEvent.as_view()), #Add or Edit an Event
    #url(r'^login/', views.login),
    url(r'^send-feedback/', views.sendFeedback.as_view()), #Send Feedback
    url(r'^admin/', include(admin.site.urls)),
    url(r'^update-response/',views.updateResponse.as_view()), #update response by a user for a single event
    url(r'^user-settings/',views.updateSettings.as_view()), #update response by a user for a single event

    url(r'', include('social_auth.urls')),
    url(r'^auth/$', views.home.as_view(), name='home'), #Google Auth begin Page, can directly access http://app.thyme.io/be/auth/login/google-oauth2/
    url(r'^auth/done/$', views.done.as_view(), name='done'), #Google Auth return
    url(r'^auth/error/$', views.error.as_view(), name='error'), #Google Auth error
    #url(r'^auth/logout/$', views.logout.as_view(), name='logout'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'auth/', include('social_auth.urls')),
    url(r'^sync/', views.syncCalendar.as_view()),#sync calendar of the user which is logged in
    url(r'^register/', views.registerUser.as_view()), #Register a new user
    url(r'^add-cal-entries/', views.addEventToCalendars.as_view()), #Add a particular "APP" Event to calendars of all participants which said "Y"
    #url(r'^ios-notifications/', include('ios_notifications.urls')),
    url(r'^add-device/', views.addDevice.as_view()),
    #Appery Specific #url(r'^set-device-id/', views.setDeviceId.as_view()),
    url(r'^send-push-test/', views.sendPushTest.as_view()),
    url(r'^del-event/', views.delEvent.as_view()),
    url(r'^add-ical-events/', views.addIcalEvents.as_view()),
    url(r'^validate-contacts/', views.validateContacts.as_view()),
    url(r'', include('gcm.urls')),
    #url(r'^api-token-auth/', 'rest_framework.authtoken.views.obtain_auth_token'),
    #url(r'^', include('backend.urls')),
    # Examples:
    # url(r'^$', 'thyme.views.home', name='home'),
    # url(r'^thyme/', include('thyme.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)

urlpatterns = format_suffix_patterns(urlpatterns)
