BEGIN;
CREATE TABLE `user_extended` (
    `uid` integer NOT NULL PRIMARY KEY,
    `phone` varchar(60),
    `fbid` varchar(255)
)
;
ALTER TABLE `user_extended` ADD CONSTRAINT `uid_refs_id_4009ba16` FOREIGN KEY (`uid`) REFERENCES `auth_user` (`id`);
CREATE TABLE `what` (
    `whatid` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `activity` varchar(150),
    `parentid` integer NOT NULL
)
;
CREATE TABLE `places` (
    `placeid` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `loc_desc` longtext NOT NULL,
    `longitude` double precision,
    `lattitude` double precision,
    `address` longtext
)
;
CREATE TABLE `schedules` (
    `scheduleid` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `begin` datetime NOT NULL,
    `end` datetime NOT NULL
)
;
CREATE TABLE `events` (
    `eid` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `tag` varchar(15) NOT NULL,
    `uid` integer NOT NULL,
    `title` varchar(1500) NOT NULL,
    `whatid` integer NOT NULL,
    `scheduleid` integer NOT NULL,
    `placeid` integer NOT NULL,
    `dead` varchar(3) NOT NULL
)
;
ALTER TABLE `events` ADD CONSTRAINT `placeid_refs_placeid_6f85fce0` FOREIGN KEY (`placeid`) REFERENCES `places` (`placeid`);
ALTER TABLE `events` ADD CONSTRAINT `scheduleid_refs_scheduleid_747f6173` FOREIGN KEY (`scheduleid`) REFERENCES `schedules` (`scheduleid`);
ALTER TABLE `events` ADD CONSTRAINT `whatid_refs_whatid_2c52b294` FOREIGN KEY (`whatid`) REFERENCES `what` (`whatid`);
ALTER TABLE `events` ADD CONSTRAINT `uid_refs_uid_fd4108c` FOREIGN KEY (`uid`) REFERENCES `user_extended` (`uid`);
CREATE TABLE `schedules_events` (
    `scheduleid` integer NOT NULL PRIMARY KEY,
    `eid` integer NOT NULL
)
;
ALTER TABLE `schedules_events` ADD CONSTRAINT `scheduleid_refs_scheduleid_649eb454` FOREIGN KEY (`scheduleid`) REFERENCES `schedules` (`scheduleid`);
ALTER TABLE `schedules_events` ADD CONSTRAINT `eid_refs_eid_361d32ee` FOREIGN KEY (`eid`) REFERENCES `events` (`eid`);
CREATE TABLE `rel_attendees` (
    `eid` integer NOT NULL,
    `uid` integer NOT NULL,
    `relid` integer AUTO_INCREMENT NOT NULL PRIMARY KEY
)
;
ALTER TABLE `rel_attendees` ADD CONSTRAINT `uid_refs_uid_470cc41d` FOREIGN KEY (`uid`) REFERENCES `user_extended` (`uid`);
ALTER TABLE `rel_attendees` ADD CONSTRAINT `eid_refs_eid_1e008dfc` FOREIGN KEY (`eid`) REFERENCES `events` (`eid`);
CREATE TABLE `attendees` (
    `relid` integer NOT NULL PRIMARY KEY,
    `accept_status` varchar(3) NOT NULL
)
;
ALTER TABLE `attendees` ADD CONSTRAINT `relid_refs_relid_12b835a1` FOREIGN KEY (`relid`) REFERENCES `rel_attendees` (`relid`);
CREATE TABLE `rel_cancellation` (
    `eid` integer NOT NULL,
    `uid` integer NOT NULL,
    `relid` integer AUTO_INCREMENT NOT NULL PRIMARY KEY
)
;
ALTER TABLE `rel_cancellation` ADD CONSTRAINT `uid_refs_uid_282c5300` FOREIGN KEY (`uid`) REFERENCES `user_extended` (`uid`);
ALTER TABLE `rel_cancellation` ADD CONSTRAINT `eid_refs_eid_38fd1f19` FOREIGN KEY (`eid`) REFERENCES `events` (`eid`);
CREATE TABLE `cancellation` (
    `relid` integer NOT NULL PRIMARY KEY,
    `when` datetime NOT NULL,
    `msg` longtext
)
;
ALTER TABLE `cancellation` ADD CONSTRAINT `relid_refs_relid_6ca90fb1` FOREIGN KEY (`relid`) REFERENCES `rel_cancellation` (`relid`);
CREATE TABLE `rel_considers_priority` (
    `uid_l` integer NOT NULL,
    `uid_r` integer NOT NULL,
    `relid` integer AUTO_INCREMENT NOT NULL PRIMARY KEY
)
;
ALTER TABLE `rel_considers_priority` ADD CONSTRAINT `uid_l_refs_uid_127fc768` FOREIGN KEY (`uid_l`) REFERENCES `user_extended` (`uid`);
ALTER TABLE `rel_considers_priority` ADD CONSTRAINT `uid_r_refs_uid_127fc768` FOREIGN KEY (`uid_r`) REFERENCES `user_extended` (`uid`);
CREATE TABLE `considers_priority` (
    `relid` integer NOT NULL PRIMARY KEY,
    `pr` integer NOT NULL
)
;
ALTER TABLE `considers_priority` ADD CONSTRAINT `relid_refs_relid_7b924e87` FOREIGN KEY (`relid`) REFERENCES `rel_considers_priority` (`relid`);
CREATE TABLE `rel_schedule_attendees` (
    `scheduleid` integer NOT NULL,
    `uid` integer NOT NULL,
    `relid` integer AUTO_INCREMENT NOT NULL PRIMARY KEY
)
;
ALTER TABLE `rel_schedule_attendees` ADD CONSTRAINT `scheduleid_refs_scheduleid_589f51c3` FOREIGN KEY (`scheduleid`) REFERENCES `schedules` (`scheduleid`);
ALTER TABLE `rel_schedule_attendees` ADD CONSTRAINT `uid_refs_uid_5e0d5c9c` FOREIGN KEY (`uid`) REFERENCES `user_extended` (`uid`);
CREATE TABLE `schedule_attendees` (
    `relid` integer NOT NULL PRIMARY KEY,
    `status` varchar(3) NOT NULL
)
;
ALTER TABLE `schedule_attendees` ADD CONSTRAINT `relid_refs_relid_18ad1c1f` FOREIGN KEY (`relid`) REFERENCES `rel_schedule_attendees` (`relid`);
CREATE TABLE `third_party_suggestions` (
    `tpid` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `placeid` integer NOT NULL,
    `link` varchar(1500) NOT NULL,
    `info` longtext
)
;
ALTER TABLE `third_party_suggestions` ADD CONSTRAINT `placeid_refs_placeid_6c8e0c6e` FOREIGN KEY (`placeid`) REFERENCES `places` (`placeid`);
CREATE TABLE `rel_third_party_user` (
    `whatid` integer NOT NULL,
    `uid` integer NOT NULL,
    `tpid` integer NOT NULL,
    `relid` integer AUTO_INCREMENT NOT NULL PRIMARY KEY
)
;
ALTER TABLE `rel_third_party_user` ADD CONSTRAINT `tpid_refs_tpid_352c936` FOREIGN KEY (`tpid`) REFERENCES `third_party_suggestions` (`tpid`);
ALTER TABLE `rel_third_party_user` ADD CONSTRAINT `whatid_refs_whatid_72d9c359` FOREIGN KEY (`whatid`) REFERENCES `what` (`whatid`);
ALTER TABLE `rel_third_party_user` ADD CONSTRAINT `uid_refs_uid_aa0ffc7` FOREIGN KEY (`uid`) REFERENCES `user_extended` (`uid`);
CREATE TABLE `third_party_user` (
    `status` varchar(3) NOT NULL,
    `when` datetime NOT NULL,
    `relid` integer NOT NULL PRIMARY KEY
)
;
ALTER TABLE `third_party_user` ADD CONSTRAINT `relid_refs_relid_467e0df7` FOREIGN KEY (`relid`) REFERENCES `rel_third_party_user` (`relid`);
CREATE TABLE `anon_flow` (
    `uid` integer NOT NULL PRIMARY KEY,
    `anoncode` varchar(1500) NOT NULL,
    `generated` datetime NOT NULL,
    `active` varchar(1) NOT NULL
)
;
ALTER TABLE `anon_flow` ADD CONSTRAINT `uid_refs_uid_38c1589b` FOREIGN KEY (`uid`) REFERENCES `user_extended` (`uid`);
CREATE INDEX `events_51c63bce` ON `events` (`uid`);
CREATE INDEX `events_42b85e24` ON `events` (`whatid`);
CREATE INDEX `events_b8a4909` ON `events` (`scheduleid`);
CREATE INDEX `events_6224e6e6` ON `events` (`placeid`);
CREATE INDEX `schedules_events_2310b62` ON `schedules_events` (`eid`);
CREATE INDEX `rel_attendees_2310b62` ON `rel_attendees` (`eid`);
CREATE INDEX `rel_attendees_51c63bce` ON `rel_attendees` (`uid`);
CREATE INDEX `rel_cancellation_2310b62` ON `rel_cancellation` (`eid`);
CREATE INDEX `rel_cancellation_51c63bce` ON `rel_cancellation` (`uid`);
CREATE INDEX `rel_considers_priority_2fd64ce3` ON `rel_considers_priority` (`uid_l`);
CREATE INDEX `rel_considers_priority_30abec8d` ON `rel_considers_priority` (`uid_r`);
CREATE INDEX `rel_schedule_attendees_b8a4909` ON `rel_schedule_attendees` (`scheduleid`);
CREATE INDEX `rel_schedule_attendees_51c63bce` ON `rel_schedule_attendees` (`uid`);
CREATE INDEX `third_party_suggestions_6224e6e6` ON `third_party_suggestions` (`placeid`);
CREATE INDEX `rel_third_party_user_42b85e24` ON `rel_third_party_user` (`whatid`);
CREATE INDEX `rel_third_party_user_51c63bce` ON `rel_third_party_user` (`uid`);
CREATE INDEX `rel_third_party_user_1ae027e8` ON `rel_third_party_user` (`tpid`);
COMMIT;
