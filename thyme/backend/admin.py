from models import *
from django.contrib import admin


class EventsAdmin(admin.ModelAdmin):
    list_display = ['eid', 'tag', 'dead','__unicode__']
    ordering = ['-eid']
admin.site.register(Events,EventsAdmin)
admin.site.register(Schedules)
admin.site.register(Places)
admin.site.register(UserExtended)
admin.site.register(What)
admin.site.register(RelAttendees)
admin.site.register(Attendees)
admin.site.register(Requests)
