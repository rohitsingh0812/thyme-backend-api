'''
    Copyright 2014, Thyme Labs Inc.
    
    This file is part of "Thyme Backend API".

    "Thyme Backend API" is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    "Thyme Backend API" is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with "Thyme Backend API".  If not, see <http://www.gnu.org/licenses/>.
'''
# Create your views here.


from serializers import UserSerializer,TokenSerializer,EventShortSerializer,EventLongSerializer
from rest_framework.views import APIView
from django.contrib.auth import logout as auth_logout
from rest_framework.authtoken.models import Token
from django.contrib.auth import authenticate
from django.http import HttpResponseRedirect
from django.contrib.messages.api import get_messages

from ios_notifications.models import *
from gcm.models import Device as GDevice

from social_auth.backends.pipeline.user import _ignore_field
import google

import random
import logging
import smtplib
from utils import *
# Get an instance of a logger
logger = logging.getLogger('default')

class syncCalendar(APIView):
    """
    Input: beginDate,endDate, user(from auth)
    """
    def do_work(self, request):
        if not validateAuth(request):
            return sendMsg("Life is hard. Invalid credentials.")
        req = logit(self.__class__.__name__,request)
        cstr = checkRequest(['beginDate','endDate'], request)
        if(cstr != ""):
            return sendMsg(cstr)
        try:
            beginDate = transformDT(strptime(request.DATA['beginDate'],DATETIME_FORMAT))
            endDate = transformDT(strptime(request.DATA['endDate'],DATETIME_FORMAT))
        except ValueError:
            return sendMsg("Date format error. Please conform to: " + DATETIME_FORMAT + ", e.g. 2013-08-10T00:00:00Z")
        user = request.user
        service = getCalService(user.id)
        if (service is None):
            return sendMsg("User calendar not shared")
        res = updateCalEntriesAllGCalsOwned(user,service,beginDate,endDate,True)
        ret = dict()
        ret['message'] = res
        postlogit(req,ret)
        return Response(ret, status=status.HTTP_200_OK)

    def get(self, request): #TODO don't allow get here eventually
        #return Response("Life is hard. Don't use GET. Tolko POST.", status=status.HTTP_400_BAD_REQUEST)
        return self.do_work(request)

    def post(self, request):
        return self.do_work(request)


class addEventToCalendars(APIView):
    """
    Input: evenId, user(from auth)
    """
    def do_work(self, request):
        if not validateAuth(request):
            return sendMsg("Life is hard. Invalid credentials.")
        req = logit(self.__class__.__name__,request)
        cstr = checkRequest(['eid'], request)
        if(cstr != ""):
            return sendMsg(cstr)
        
        eid = int(request.DATA['eid'])
        try:
            ev = Events.objects.get(eid=eid)
        except Events.DoesNotExist:
            return sendMsg("Event ID is invalid",1)
        
        if(ev.dead == "-2"):
            return sendMsg("Already sent the calendar invites",1)
        #Push notifications sent, add to the calendars of the users
        user = request.user
        num=0
        if (user != ev.uid.uid): #only initiator can call this
            return sendMsg("Only initiator of the event allowed to add events to calendar")
        ulist = getAttendees(ev)
        service = getCalService(user.id)
        if(service):
            fname = user.first_name
            lname = user.last_name
            gid = addEvent(ev,service,user,ulist)
            
            
        for u in list(set(ulist)):
            #Add event to this calendar
            #send_push if possible
            if(u != user):
                #push_final(u,ev.uid.uid,ev) #don't send to initiator
                msg = get_name(user) + " finalized an event!#" + str(ev.eid) 
                do_push_one_msg(u,msg,ev) #don't send to initiator
        ev.dead = "-2"
        ev.save()
        ret = dict()
        if(service):
            ret['message'] = "Added event to " + fname + " " + lname + "'s calendar"
        else:
            ret['message'] = "Couldn't access gcal for initiator."
        postlogit(req,ret)
        return Response(ret, status=status.HTTP_200_OK)

    def get(self, request): #TODO don't allow get here eventually
        #return Response("Life is hard. Don't use GET. Tolko POST.", status=status.HTTP_400_BAD_REQUEST)
        return self.do_work(request)

    def post(self, request):
        return self.do_work(request)

class getAllEventsFromApp(APIView):
    """
    Input: num_entries, Time(all starting at or beyond this time)
    Behaviour: may give more entries if the last one has multiple events at the same time
    Output: num_entries values in the LIST {EID,title, status, beginTime, EndTime}
    """
    def do_work(self, request):
        if not validateAuth(request):
            return sendMsg("Life is hard. Invalid credentials.")
        req = logit(self.__class__.__name__,request)
        #uid = int(request.DATA['uid'])
        cstr = checkRequest(['numEntries','time'], request)
        if(cstr != ""):
            return sendMsg(cstr)

        #assert request.user.id == uid
        numEntries = int(request.DATA['numEntries'])
        try:
            time = transformDT(strptime(request.DATA['time'],DATETIME_FORMAT))
        except ValueError:
            return sendMsg("Date format error. Please conform to: " + DATETIME_FORMAT + ", e.g. 2013-08-10T00:00:00Z")
        #find all events for this user
        evs = Events.objects.filter(tag='APP',scheduleid__begin__gt=time).filter(Q(uid__uid=request.user) | Q(relattendees__uid__uid=request.user)).filter(~Q(dead="1")).distinct().order_by('scheduleid__begin')[:numEntries]

        serializer = EventShortSerializer(evs,many=True)
        postlogit(req,serializer.data)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def get(self, request): #TODO don't allow get here eventually
        #return Response("Life is hard. Don't use GET. Tolko POST.", status=status.HTTP_400_BAD_REQUEST)
        return self.do_work(request)

    def post(self, request):
        return self.do_work(request)

class getAllEvents(APIView):
    """
    Input: num_entries, Time(all starting at or beyond this time)
    Behaviour: may give more entries if the last one has multiple events at the same time
    Output: num_entries values in the LIST {EID,title, status, beginTime, endTime} as nested Model outputs
    """

    def do_work(self, request):
        if not validateAuth(request):
            return sendMsg("Life is hard. Invalid credentials.")
        req = logit(self.__class__.__name__,request)
        #uid = int(request.DATA['uid'])
        #assert request.user.id == uid
        cstr = checkRequest(['numEntries','time'], request)
        if(cstr != ""):
            return sendMsg(cstr)

        numEntries = int(request.DATA['numEntries'])
        try:
            time = transformDT(strptime(request.DATA['time'],DATETIME_FORMAT))
        except ValueError:
            return sendMsg("Date format error. Please conform to: " + DATETIME_FORMAT + ", e.g. 2013-08-10T00:00:00Z")
        #find all events for this user
        evs = Events.objects.filter(scheduleid__begin__gt=time).filter(Q(uid__uid=request.user) | Q(relattendees__uid__uid=request.user)).filter(~Q(dead="1")).distinct().order_by('scheduleid__begin')[:numEntries]
        serializer = EventShortSerializer(evs,many=True)
        postlogit(req,serializer.data)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def get(self, request): #TODO don't allow get here eventually
        #return Response("Life is hard. Don't use GET. Tolko POST.", status=status.HTTP_400_BAD_REQUEST)
        return self.do_work(request)

    def post(self, request):
        return self.do_work(request)

class getEventDetails(APIView):
    """
    Inputs: EID
    Outputs:Initiator UID,fname,lname,email,status, LIST {UserInfo:[UID,email,phone,fname,lname,status]} OtherUsers,
    beginTime, endTime, WHATID/EventType, Title, (placeid, loc_desc,long,latt,address) 	[7am-12 midnight]
    """
    def do_work(self, request):
        if not validateAuth(request):
            return sendMsg("Life is hard. Invalid credentials.")
        req = logit(self.__class__.__name__,request)
        cstr = checkRequest(['eid'], request)
        if(cstr != ""):
            return sendMsg(cstr)

        eid = request.DATA['eid']

        #find all details for this event
        evs = Events.objects.get(eid=eid)
        if (evs.uid.uid != request.user):
            if (not RelAttendees.objects.filter(eid=evs,uid=request.user)):
                return sendMsg("You don't have access to this event since the authenticated user is not an attendee")
        serializer = EventLongSerializer(evs)
        postlogit(req,serializer.data)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def get(self, request): #TODO don't allow get here eventually
        #return Response("Life is hard. Don't use GET. Tolko POST.", status=status.HTTP_400_BAD_REQUEST)
        return self.do_work(request)

    def post(self, request):
        return self.do_work(request)

class getToken(APIView):
    """
    Authenticates a user with given username/password and gives back the token
    """
    def do_work(self, request):
        #Authenticated
        cstr = checkRequest(['username','password'], request)
        if(cstr != ""):
            return sendMsg(cstr)
        username = request.DATA['username']
        password = request.DATA['password']
        user = authenticate(username=username, password=password)
        # t = Token.objects.get(user=user.id)
        # serializer = TokenSerializer(t)
        # return Response(serializer.data, status=status.HTTP_200_OK)

        if user is not None:
            if user.is_active:
                # OutputToken
                serializer = TokenSerializer(Token.objects.get(user=user.id))
                return Response(serializer.data, status=status.HTTP_200_OK)
            else:
                return sendMsg("Life is hard. Disabled Account.")
                    # Return a 'disabled account' error message
        else:
            return sendMsg("Life is hard. Invalid credentials.")
            # Return an 'invalid login' error message.

    def get(self, request): #TODO don't allow get here eventually
        #return Response("Life is hard. Don't use GET. Tolko POST.", status=status.HTTP_400_BAD_REQUEST)
        return self.do_work(request)

    def post(self, request):
        return self.do_work(request)

class TokenUid(APIView):
    """
    Checks if Token is valid and returns a uid
    """
    def do_work(self, request):
        #Authenticated 
        if not validateAuth(request):
            return sendMsg("Life is hard. Invalid credentials.")
        req = logit(self.__class__.__name__,request)
        uext = UserExtended.objects.get(uid=request.user.id)
        
        #GCAL SYNC PART
        dToday = datetime.datetime.now()
        dToday.replace(hour=0,minute=0,second=0)
        dBegin = dToday - timedelta(days=2)
        dEnd = dToday + timedelta(days=30)
        syncGCal(uext.uid,dBegin,dEnd)
        #syncGCal(user,dBegin,dEnd)
        
        
        
        serializer = UserSerializer(uext)
        #serializer = UserSerializer(request.user)
        postlogit(req,serializer.data)
        return Response(serializer.data, status=status.HTTP_200_OK)
        #return Response([request.user.id], status=status.HTTP_200_OK)

    def get(self, request):
        #return Response("Life is hard. Don't use GET. Tolko POST.", status=status.HTTP_400_BAD_REQUEST)
        return self.do_work(request)
                
    def post(self, request):
        return self.do_work(request)
        #check if key is valid and for which user
        #try:
        #    uid = Token.objects.get(key=request.key).user
        #except Token.DoesNotExist:
        #    return Response("Life is ploho. Bad Token Bro.", status=status.HTTP_400_BAD_REQUEST)
        #serializer = UserSerializer(key=request.key,user=uid)
        #if serializer.is_valid():
        #    return Response(serializer.user, status=status.HTTP_201_CREATED)
        #return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

def do_push_one_msg_gcm(u,msg):
    logger.debug("GCM Call: " + u.username + "|" + msg)
    ds = GDevice.objects.filter(dev_id = str(u.id))
    if(ds.exists()):#Updates device eg_id
        d = ds.get()
        #d.send_message(str(random.randint(10000,50000)) + msg)
        r = send_gcm(
    api_key=settings.GCM_APIKEY,
    regs_id=[d.reg_id],
    data={'msg': msg, 'title' : 'Push from Thyme BE'},
    collapse_key="message")
        logger.debug("GCM Call: " + u.username + "|" + msg + "| Sent=" + json.dumps(r) + " | " + str(d.reg_id) )
        return True
    else:
        logger.debug("GCM Call: " + u.username + "|" + msg + "| NO_DEVICE_FOUND")
        return False

def do_push_one_msg_ios(u,msg):
    srv = APNService.objects.filter(id = 1).get()
    devices = Device.objects.filter(users=u,service=srv)
    if devices.exists():
        notification = Notification.objects.create(message=msg, service=srv)
        srv.push_notification_to_devices(notification, devices, chunk_size=200) #RTODO Chunk Size
        #return sendMsg("Message sent successfully: " + msg + " to: " + devices.get().token,1)
        return True
    else:
        return False


def do_push_one_msg(u,msg,e):
    r1 = do_push_one_msg_gcm(u,msg)
    r2 = do_push_one_msg_ios(u,msg)
    if((not r1) and (not r2)):
        #send email to user
        details = str(e)
        sendEmail(u,msg,"Event details Created/Updated:\n" + details + "\nVisit your \"My Invites\" page on the application for more details and accepting the invite!")
    
def do_push_one(u,userInit,e):
    msg = get_name(userInit) + " invites you: " + wid2str(e.whatid) + " on "+ e.scheduleid.begin.strftime('%A, %d %b') +" - " + e.title + "#" + str(e.eid)
    do_push_one_msg(u,msg,e)

def do_push(userList,userInit,e):
    for u in list(set(userList)):
        do_push_one(u,userInit,e)
            
class calcEventCreateOrEdit(object):
    def __init__(self, isAnEdit, uid, whatId, title, otherUsers, selectedBegin, selectedEnd, placeId, locDesc, eid,isconf,confnum,notes):
#IsAnEdit?, UID, WHATID, TITLE, LIST {UserInfo:UID/email/phone} otherUsers, SelectedTime {date,begin,end}, PLACEID (or -1), LOC_DESC(Ignored if PLACEID is not -1), EID (if edit)
        self.isAnEdit = isAnEdit #Bool
        self.uid = uid #Int
        self.whatId = whatId #Int
        self.title = title #Int
        self.otherUsers = otherUsers #List of dictionaries [[uid:?,email:?,phone:?,fbid:?,first_name:?,last_name:?]]
        self.selectedBegin = selectedBegin #DateTime
        self.selectedEnd = selectedEnd #DateTime
        self.placeId = placeId #Int
        self.locDesc = locDesc #Int
        self.eid = eid #Int, only in case of edit
        self.isconf = isconf
        self.confnum = confnum
        self.notes = notes

    def do_work(self):
        if(DBG):
            print "isAnEdit: %d",self.isAnEdit
        userInit = UserExtended.objects.get(uid=self.uid)
        what = What.objects.get(whatid=self.whatId)
        if(self.selectedBegin >= self.selectedEnd):
            return "selected begin date should be strictly lesser than selected end date"
        if(not self.isAnEdit):
            #If some users are not in the system, then create new user IDs for them and temp passwords. Otherwise search for UIDs and get them for each person in UserInfo List.
            sre = updateUIDs(self.otherUsers)
            if(sre !=""):
                return sre
            #Now we have all uid/user's needed to make the entry
            for userInfo in self.otherUsers.values():
                if (not ('user' in userInfo)) or (not ('uid' in userInfo)):
                    return "user or uid field not set in userInfo"

            #Add Entry in PLACES if PLACEID == -1 and LOC_DESC is non-empty, so that it comes in search next time. Avoid Duplicaction here. Otherwise no Entry if PLACEID is set or can find exact loc_desc string.
            place = addNewPlaceIfNotExists(self.placeId,self.locDesc)

            #Add an entry in SCHEDULES table {begin, end} [date is assumed in begin/end]... based on SelectedTime and also relate it  to this event in Schedules_Event Table (RTODO later make efficient with schedule search)
            schd = newSchedule(self.selectedBegin,self.selectedEnd)
            evs = Events.objects.filter(uid=self.uid,scheduleid=schd)
            if(evs.exists()):
                for evi in evs:
                    if(evi.title == self.title):
                        return "Already created event with same title and schedule."
            #Add ROW in EVENTS table use proper PLACEID
            e = Events(tag="APP",uid=userInit,title=self.title,whatid=what,scheduleid=schd,placeid=place,dead="-1",isconf=self.isconf,confnum=self.confnum,notes=self.notes)
            e.save()

            #Multiple Attendees and RelAttendees Table Entries with STATUS = U
            #The initiator is definitely attending? Status = Y
            #initiator
            addOrUpdateAttendee(e,userInit,"Y")
            #Others
            for userInfo in self.otherUsers.values():
                addOrUpdateAttendee(e,userInfo['user'],"U")

            userList = []
            for userInfo in self.otherUsers.values():
                if not (userInfo['user'] in userList):
                    userList.append(userInfo['user'].uid)
            do_push(userList,userInit.uid,e)
            return ""
            #[Later] RelThirdPartyUser table Entry if the user selected a place that is in the ThirdPartySuggestions Table

            #[Not Needed, we know this coz scheduleid <-> eid is one-one] Multiple entries in ScheduleAttendees [who all are invited to decide on this schedule item] and RelScheduleAttendees with Status = U

            #SEND PUSH notifications to APP users + emails/text [later] if they configure so.
            #RTODO Figure out how to do this PUSH. This should lead to their Notifications page

            #[Later]IMP: For those not using the app: Create an Entry in table AnonFlow and generate + send the link to them via Email/Text (Use FE resources for sending Text).The Link should Auto-login them into the web app and take them to invites screen. thyme.com/anon/<AnonCode>. This should work unless they change password or complete profile (Note: we have their phone/email already). ARTODO Decide. Alpha: Assume everyone registered? Template for Django generated page, How to send them the link, BE can send emails, FE can send SMSs [Later in Beta]
        else:
            #Its an edit, so change stuff now!
            #self.eid is the event to be modified!
            (found,ev) = getEventIfExists(self.eid)
            if not found: #You must have been able to find the event
                return "Couldn't find event id = " + str(self.eid)
            sre = updateUIDs(self.otherUsers)
            if(sre !=""):
                return sre
            for userInfo in self.otherUsers.values():
                if (not ('uid' in userInfo)) or (not ('user' in userInfo)):
                    return "uid or user not found in userInfo"

            #Add Entry in PLACES if PLACEID == -1 and LOC_DESC is non-empty, so that it comes in search next time. Avoid Duplicaction here. Otherwise no Entry if PLACEID is set or can find exact loc_desc string.
            place = addNewPlaceIfNotExists(self.placeId,self.locDesc)


            #Add an entry in SCHEDULES table {begin, end} [date is assumed in begin/end]... based on SelectedTime and also relate it  to this event in Schedules_Event Table (RTODO later make efficient with schedule search)
            schd = newSchedule(self.selectedBegin,self.selectedEnd)
            if not(ev.uid == userInit):
                return "Only the initiator can edit an event"
            ev.title = self.title
            ev.whatid = what
            ev.scheduleid = schd
            ev.placeid = place
            ev.dead="-1"
            ev.isconf = self.isconf
            ev.confnum = self.confnum
            ev.notes = self.notes
            ev.save()#TODO check that this does only update

            #Multiple Attendees and RelAttendees Table Entries with STATUS = U
            #The initiator is definitely attending? Status = Y
            #initiator
            addOrUpdateAttendee(ev,userInit,"Y")
            #Others
            for userInfo in self.otherUsers.values():
                addOrUpdateAttendee(ev,userInfo['user'],"U")

            userList = []
            for userInfo in self.otherUsers.values():
                if not (userInfo['user'] in userList):
                    userList.append(userInfo['user'].uid)#user objects
            do_push(userList,userInit.uid,ev)
            #[Later] RelThirdPartyUser table Entry if the user selected a place that is in the ThirdPartySuggestions Table
            return ""
            #[Not Needed, we know this coz scheduleid <-> eid is one-one] Multiple entries in ScheduleAttendees [who all are invited to decide on this schedule item] and RelScheduleAttendees with Status = U

            #SEND PUSH notifications to APP users + emails/text [later] if they configure so.
            #RTODO Figure out how to do this PUSH. This should lead to their Notifications page

            #[Later]IMP: For those not using the app: Create an Entry in table AnonFlow and generate + send the link to them via Email/Text (Use FE resources for sending Text).The Link should Auto-login them into the web app and take them to invites screen. thyme.com/anon/<AnonCode>. This should work unless they change password or complete profile (Note: we have their phone/email already). ARTODO Decide. Alpha: Assume everyone registered? Template for Django generated page, How to send them the link, BE can send emails, FE can send SMSs [Later in Beta]

class validateContacts(APIView):
    """
    Get all contacts that are actually in our database
    Inputs: LIST {UserInfo:UID/email/phone} otherUsers
    Output: Another JSON list of users
    """
    def do_work(self, request):
        #Authenticated
        if not validateAuth(request):
            return sendMsg("Life is hard. Invalid credentials.")
        req = logit(self.__class__.__name__,request)
        #Get stuff from request.DATA
        cstr = checkRequest(['otherUsers'], request)
        if(cstr != ""):
            return sendMsg(cstr)

        otherUsers = request.DATA['otherUsers']
        delks = []
        for k,userInfo in otherUsers.iteritems(): #ideally have email/phone and name for user acc creation later
                if not (('phone' in userInfo) or ('email' in userInfo)):
                    #return sendMsg("phone/email missing in userInfo validateContacts")
                    delks.append(k)
        for k in delks:
            del otherUsers[k]
        usersNew = findUsers(otherUsers)
        postlogit(req,usersNew)
        return Response(usersNew,status=status.HTTP_200_OK)
        #Now we have all uid/user's needed to make the entry
        #return sendMsg("Successfully created the event.",1)

    def get(self, request):
        #return Response("Life is hard. Don't use GET. Tolko POST.", status=status.HTTP_400_BAD_REQUEST)
        return self.do_work(request)

    def post(self, request):
        return self.do_work(request)


class createOrEditEvent(APIView):
    """
    Create or edit event
    Inputs: #IsAnEdit?, UID, WHATID, TITLE, LIST {UserInfo:UID/email/phone} otherUsers, SelectedTime {date,begin,end}, PLACEID (or -1), LOC_DESC(Ignored if PLACEID is not -1), EID (if edit)
    Input: Events Long Serializer format + List of users
    date = { event: {} users: { {} {} ... }
    """
    def do_work(self, request):
        #Authenticated
        if not validateAuth(request):
            return sendMsg("Life is hard. Invalid credentials.")
        req = logit(self.__class__.__name__,request)
        #Get stuff from request.DATA
        cstr = checkRequest(['isAnEdit','whatid','title','placeid','locDesc','otherUsers','selectedBegin','selectedEnd','eid'], request)
        if(cstr != ""):
            return sendMsg(cstr)
        #'isconf','confnum' and 'notes' are optional
        isAnEdit = (request.DATA['isAnEdit'] == "True")
        uid = request.user.id#int(request.DATA['uid'])
        whatId = int(request.DATA['whatid'])
        if(whatId == 0):
            whatId = 1
        title = request.DATA['title']
        placeId = int(request.DATA['placeid'])
        locDesc = request.DATA['locDesc']
        #otherUsers is an array
        otherUsers = request.DATA['otherUsers']
        for userInfo in otherUsers.values(): #ideally have email/phone and name for user acc creation later
                if not (('uid' in userInfo) or ('phone' in userInfo) or ('email' in userInfo)):
                    return sendMsg("uid/phone/email all missing in userInfo createEvent")
        try:
            selectedBegin = transformDT(strptime(request.DATA['selectedBegin'],DATETIME_FORMAT))
            selectedEnd = transformDT(strptime(request.DATA['selectedEnd'],DATETIME_FORMAT))
        except ValueError:
            return sendMsg("Date format error. Please conform to: " + DATETIME_FORMAT + ", e.g. 2013-08-10T00:00:00Z")
        if(selectedBegin >= selectedEnd):
            return sendMsg("selected begin date should be strictly lesser than selected end date")
        eid = int(request.DATA['eid'])
        isconf='N'
        confnum=''
        notes=''
        
        if('isconf' in request.DATA):
            isconf = request.DATA['isconf']
            if(isconf != 'Y' and isconf != 'N'):
                return sendMsg("isconf should be Y or N")
        
        if('confnum' in request.DATA):
            confnum = request.DATA['confnum']
        
        if('notes' in request.DATA):
            notes = request.DATA['notes']
        
        postlogit(req,"Parsing Done")
            
        xx = calcEventCreateOrEdit(isAnEdit, uid, whatId, title, otherUsers, selectedBegin, selectedEnd, placeId, locDesc, eid,isconf,confnum,notes)
        errm = xx.do_work()
        if (errm != ""):
            return sendMsg(errm)
        #need to send push notifications here
        
        if(isAnEdit):
            postlogit(req,"Edit Successful")
            return sendMsg("Successfully edited the event.",1)
        else:
            postlogit(req,"Create Successful")
            return sendMsg("Successfully created the event.",1)

    def get(self, request):
        #return Response("Life is hard. Don't use GET. Tolko POST.", status=status.HTTP_400_BAD_REQUEST)
        return self.do_work(request)

    def post(self, request):
        return self.do_work(request)

#TODO Check that there's no issue with using id's instead of actual user objects etc (efficiency? correctness?)
#For now, keep the matrix calculation a part of main algo
#Find time given some constraints
class findThyme(APIView):
    """
    Find time slots given constraints
    UID, WHATID, duration, dateBegin, dateEnd, number_timeslots_per_day, LIST {UserInfo:[UID/email/phone]} OtherUsers
    change to listOfDates later
    """
    def do_work(self, request,cal):
        #Authenticated
        if(cal == "0" or cal == "2"):
            CALACT = True
            #return sendMsg("CalBasedBitVectors disabled for now")
        else:
            CALACT = False
        
            
        if not validateAuth(request):
            return sendMsg("Life is hard. Invalid credentials.")
        req = logit(self.__class__.__name__,request)
        cstr = checkRequest(['duration','whatid','numPerDay','otherUsers','dateBegin','dateEnd'], request)
        if(cstr != ""):
            return sendMsg(cstr)
        
        uid = request.user.id
        uidList = [uid]
        whatId = int(request.DATA['whatid'])
        duration = int(request.DATA['duration']) #Minutes
        try:
            dateBegin = transformDT(strptime(request.DATA['dateBegin'],DATETIME_FORMAT)) #Assume EST for now
            dateEnd = transformDT(strptime(request.DATA['dateEnd'],DATETIME_FORMAT))
        except ValueError:
            return sendMsg("Date format error. Please conform to: " + DATETIME_FORMAT + ", e.g. 2013-08-10T00:00:00Z")
        
        evLst=[]
        if(cal == "2"):
            #Means there are busy times sent in the list
            cstr = checkRequest(['events'], request)
            if(cstr != ""):
                return sendMsg(cstr)
            evRawLst = request.DATA['events']
            for i in evRawLst:
                try:
                    dbeg = (transformDT(strptime(i['begin'],DATETIME_FORMAT)))
                    dend = (transformDT(strptime(i['end'],DATETIME_FORMAT)))
                except ValueError:
                    return sendMsg("Date format error. Please conform to: " + DATETIME_FORMAT + ", e.g. 2013-08-10T00:00:00Z")
                except:
                    return sendMsg("Event List format error. [{begin -> date1, end -> date2},{},...]")
                evLst.append((dbeg,dend))
            addICEventsNoDel(evLst,request.user.userextended) 
        else:
            #GCAL SYNC for these days needed
            syncGCalNoDel(request.user,dateBegin,dateEnd)
        numPerDay = int(request.DATA['numPerDay'])
        #print "Analyzing otherUsers"
        otherUsers = request.DATA['otherUsers']
        for userInfo in otherUsers.values(): #ideally have email/phone and name for user acc creation later
            if not (('uid' in userInfo) or ('phone' in userInfo) or ('email' in userInfo)):
                return sendMsg("entries in otherUsers should have at least one of uid,phone,email fields")
        #If some users are not in the system, then create new user IDs for them and temp passwords. Otherwise search for UIDs and get them for each person in UserInfo List.
        sre = updateUIDs(otherUsers)
        if(sre !=""):
            return sendMsg(sre)
        #Now we have all uid's needed to make the entry
        for userInfo in otherUsers.values():
            if not ('uid' in userInfo):
                if('email' in userInfo):
                    return sendMsg("Sorry, user not present: " + userInfo['email'])
                elif ('phone' in userInfo):
                    return sendMsg("Sorry, user not present: " + userInfo['phone'])
            uidList.append(userInfo['uid'])
        uidList = list(set(uidList))
        #startSearchTime = strptime(request.DATA['startSearchTime'],DATETIME_FORMAT)
        #endSearchTime = strptime(request.DATA['endSearchTime'],DATETIME_FORMAT) #As of now, its 6am to 11:30pm
        #For each day
        
        dt= dateEnd
        if(dt.hour != 0)  or (dt.minute != 0) or (dt.second != 0):
            return sendMsg("hour/minute/seconds should all be zero for date inputs")

        dt = dateBegin
        dToday = datetime.datetime.now()
        dtToday = datetime.datetime(dToday.year,dToday.month,dToday.day).replace(tzinfo=ltz)
        if(dateBegin < dtToday):
            dt=dtToday
        #print "Parsed Request!"
        if(dt.hour != 0)  or (dt.minute != 0) or (dt.second != 0):
            req.result = "hour/minute/seconds should all be zero for date inputs"
            req.save()
            return sendMsg("hour/minute/seconds should all be zero for date inputs")
        resL = dict()
        calServ = dict()
        if(CALACT):#CalServices
            for uid in uidList:
                calServ[str(uid)] =  getCalService(uid)
        while (dt <= dateEnd):
            lstSlots = getFreeThymes(uidList,dt,duration,whatId,numPerDay,calServ,CALACT,evLst,req)
            tres = dict()
            j=0
            for i in lstSlots:
                ttres = dict()
                dtl = dt + timedelta(minutes=15*i)
                dtr = dtl + timedelta(minutes=duration)
                ttres["0"] = dtl.strftime("%H:%M")
                ttres["1"] = dtr.strftime("%H:%M")
                ttres["2"] = dtl.strftime(DATETIME_FORMAT)
                ttres["3"] = dtr.strftime(DATETIME_FORMAT)
                tres[str(j)] = ttres
                j=j+1
            resL[dt.strftime('%A ' + DATETIME_FORMAT)] = tres
            dt = dt + timedelta(hours=24)
        #Need to return resL
        if(DBG):
            print resL
        postlogit(req,resL)
        return Response(resL,status=status.HTTP_200_OK)

    def get(self, request, cal):
        #return Response("Life is hard. Don't use GET. Tolko POST.", status=status.HTTP_400_BAD_REQUEST)
        return self.do_work(request,cal)

    def post(self, request,cal):
        if(DBG):
            print request.DATA
        return self.do_work(request,cal)

def push_update(u,userInit,e,status):
    if(status == "Y"):
        msg = get_name(u) + " accepted the event: " + wid2str(e.whatid) + " on "+ e.scheduleid.begin.strftime('%A, %d %b') +" - " + e.title + "#" + str(e.eid)
    else:
        msg = get_name(u) + " can't make it to the event: " + wid2str(e.whatid) + " on "+ e.scheduleid.begin.strftime('%A, %d %b') +" - " + e.title + "#" + str(e.eid)

    #gdevs = GDevice.objects.filter(dev_id=userInit.id)
    #if gdevs.exists():
    #    for d in gdevs:
    #        send_push(d,msg)
    #else:
        #send email to user
    #    details = str(e)
    #    sendEmail(userInit,msg,"Event update:\n" + details + "\nVisit your \"My Invites\" page on the application for more details.")
    do_push_one_msg(userInit,msg,e)
    
class addIcalEvents(APIView): #IMPORTANT RTODO TODO: Remove events that were there earlier in the DB but aren't there in the calendar anymore
    """
    the eventlist sent from iOS devices is put into database here
    """
    def do_work(self,request):
        #Authenticated
        if not validateAuth(request):
            return sendMsg("Life is hard. Invalid credentials.")
        req = logit(self.__class__.__name__,request)
        #uid = request.user.id
        evLst =[]
        
        cstr = checkRequest(['events'], request)
        if(cstr != ""):
            return sendMsg(cstr)
        evRawLst = request.DATA['events']
        for i in evRawLst:
            try:
                dbeg = (transformDT(strptime(i['begin'],DATETIME_FORMAT)))
                dend = (transformDT(strptime(i['end'],DATETIME_FORMAT)))
            except ValueError:
                return sendMsg("Date format error. Please conform to: " + DATETIME_FORMAT + ", e.g. 2013-08-10T00:00:00Z")
            except:
                return sendMsg("Event List format error. [{begin -> date1, end -> date2},{},...]")
            evLst.append((dbeg,dend))
        k = addICEvents(evLst,request.user.userextended)
        postlogit(req,k)
        return sendMsg("Successfully added " + str(k) + " events", 1)
    
    def get(self, request):
        #return Response("Life is hard. Don't use GET. Tolko POST.", status=status.HTTP_400_BAD_REQUEST)
        return self.do_work(request)

    def post(self, request):
        return self.do_work(request)

class updateResponse(APIView):
    """
    A user updates their response to a schedule (event=schedule in Alpha)
    Inputs:  eid, status(Y/N)
    In future: list <scheduleid, status>
    """
    def do_work(self,request):
        #Authenticated
        if not validateAuth(request):
            return sendMsg("Life is hard. Invalid credentials.")
        req = logit(self.__class__.__name__,request)
        #uid = request.user.id
        cstr = checkRequest(['eid','status'], request)
        if(cstr != ""):
            return sendMsg(cstr)

        eid = int(request.DATA['eid'])
        statusr = request.DATA['status']
        if not ((statusr == "Y") or (statusr == "N")) :
            return sendMsg("status should be Y or N")
        e=Events.objects.get(eid=eid)
        if(e.dead == "-2"): #Event has been finalized
            return sendMsg("Event has already been finalized! Can't change response.",1)
        user=request.user.userextended
        addOrUpdateAttendee(e,user,statusr)
        push_update(user.uid,e.uid.uid,e,statusr)
        res = dict()
        res['message'] = "Successfully changed response for " + user.uid.username + " to " + statusr
        postlogit(req,res)
        return Response(res,status=status.HTTP_200_OK)


    def get(self, request):
        #return Response("Life is hard. Don't use GET. Tolko POST.", status=status.HTTP_400_BAD_REQUEST)
        return self.do_work(request)

    def post(self, request):
        return self.do_work(request)


class delEvent(APIView):
    """
    The initiator can delete the event
    """
    def do_work(self,request):
        #Authenticated
        if not validateAuth(request):
            return sendMsg("Life is hard. Invalid credentials.")
        req = logit(self.__class__.__name__,request)
        cstr = checkRequest(['eid'], request)
        if(cstr != ""):
            return sendMsg(cstr)

        eid = int(request.DATA['eid'])
        user = request.user
        try:
            e=Events.objects.get(eid=eid)
        except Events.DoesNotExist:
            return sendMsg("Invalid Event ID")
        if(e.uid.uid != request.user):
            return sendMsg("Only initiators can delete events",1)
        #if(e.dead == "-2"): #Event has been finalizes
        #    return sendMsg("Event has already been finalized! Can't delete it.",1)
        e.delete()
        res = dict()
        res['message'] = "Successfully Deleted Event!"
        postlogit(req,res)
        return Response(res,status=status.HTTP_200_OK)


    def get(self, request):
        #return Response("Life is hard. Don't use GET. Tolko POST.", status=status.HTTP_400_BAD_REQUEST)
        return self.do_work(request)

    def post(self, request):
        return self.do_work(request)


class sendFeedback(APIView):
    """
    Emails feedback to team@thymelabs.com
    """
    def do_work(self,request):
        if not validateAuth(request):
            return sendMsg("Life is hard. Invalid credentials.")
        req = logit(self.__class__.__name__,request)
        cstr = checkRequest(['feedback'], request)
        if(cstr != ""):
            return sendMsg(cstr)

        feedback = request.DATA['feedback']
        #Send Email
        try:
            send_mail("Alpha Feedback from "+ request.user.username, feedback, request.user.email, ["team@thymelabs.com"])
            msg = "NO MO Thanks for your feedback!"
            return sendMsg(msg,1)
        except smtplib.SMTPException:
            msg = "Email not sent :("
            return sendMsg(msg)


    def get(self, request):
        #return Response("Life is hard. Don't use GET. Tolko POST.", status=status.HTTP_400_BAD_REQUEST)
        return self.do_work(request)

    def post(self, request):
        return self.do_work(request)

class home(APIView):
    """Home view, displays login mechanism"""
    def do_work(self,request):
        if request.user.is_authenticated():
            return HttpResponseRedirect('auth/done')
        else:
            return render(request,'home.html')
    def get(self, request):
        #return Response("Life is hard. Don't use GET. Tolko POST.", status=status.HTTP_400_BAD_REQUEST)
        return self.do_work(request)

    def post(self, request):
        return self.do_work(request)

class done(APIView):
    """Login complete view, displays user data"""
    def do_work(self,request):
        if("code" in request.GET):
            return render(request, "empty.html")
        #TODO Make secure later: one time show based on "user" value in  session: one time hash
        if not ("user" in request.session):
            return sendMsg("Life is hard. Invalid credentials.")
        request.user = User.objects.get(id=request.session["user"])
        req = logit(self.__class__.__name__,request)
        #user = User.objects.get(id=request.session["user"])
        #Remove the session variable
        del request.session['user']
        #return Response(user.auth_token.key, status=status.HTTP_200_OK)
        #return render(request,'done.html')

        return HttpResponseRedirect('/be/auth/done?code=' + request.user.auth_token.key)

        #return render(request,'empty.html')

    def get(self, request):
        #return Response("Life is hard. Don't use GET. Tolko POST.", status=status.HTTP_400_BAD_REQUEST)
        return self.do_work(request)

    def post(self, request):
        return self.do_work(request)

class error(APIView):
    """Error view"""
    def do_work(self, request):
        messages = get_messages(request)
        return sendMsg(messages)

    def get(self, request):
        #return Response("Life is hard. Don't use GET. Tolko POST.", status=status.HTTP_400_BAD_REQUEST)
        return self.do_work(request)

    def post(self, request):
        return self.do_work(request)

class logout(APIView):
    """Logout and go back to home"""
    def do_work(self, request):
        del request.session['user']
        auth_logout(request)
        return HttpResponseRedirect('/')

    def get(self, request):
        #return Response("Life is hard. Don't use GET. Tolko POST.", status=status.HTTP_400_BAD_REQUEST)
        return self.do_work(request)

    def post(self, request):
        return self.do_work(request)

class registerUser(APIView):
    """
    Inputs: email,username, password,phone
    """
    def do_work(self,request):
        cstr = checkRequest(['email','username','phone','password'], request)
        if(cstr != ""):
            return sendMsg(cstr)
        req = logitreq(self.__class__.__name__,request)
        email = request.DATA['email']
        username = request.DATA['username']
        phone = sanctify(request.DATA['phone'])
        password = request.DATA['password']
        if(User.objects.filter(email=email)):
            return sendMsg("Email already in use")
        elif (User.objects.filter(username=username)):
            return sendMsg("Username already in use")
        elif (User.objects.filter(userextended__phone=phone)):
            return sendMsg("Phone already in use")

        u = User.objects.create_user(username=username,email=email,password=password)
        #Token automatically generated and to be sent now
        ue = UserExtended(uid=u,phone=phone)
        ue.save()
        serializer = TokenSerializer(Token.objects.get(user=u))
        postlogit(req,serializer.data)
        return Response(serializer.data, status=status.HTTP_200_OK)
        # rd = dict()
        # rd['message'] = "Successfully created user: " + username
        # return Response(["Successfully changed response for " + user.uid.username + " to " + status],status=status.HTTP_200_OK)


    def get(self, request):
        #return Response("Life is hard. Don't use GET. Tolko POST.", status=status.HTTP_400_BAD_REQUEST)
        return self.do_work(request)

    def post(self, request):
        return self.do_work(request)

class updateSettings(APIView):
    """
    Inputs: email,newPassword,phone,fname,lname
    """
    def do_work(self,request):
        #Authenticated
        if not validateAuth(request):
            return sendMsg("Life is hard. Invalid credentials.")
        req = logit(self.__class__.__name__,request)
        cstr = checkRequest(['phone','fname','lname','newPassword'], request)
        if(cstr != ""):
            return sendMsg(cstr)

        phone = sanctify(request.DATA['phone'])
        ophone = request.user.userextended.phone
        isUniquePhone = (phone == ophone or (not UserExtended.objects.filter(phone=phone)))
        fname = request.DATA['fname']
        lname = request.DATA['lname']
        user = request.user

        ofname = user.first_name
        olname = user.last_name
        npass= request.DATA['newPassword']
        changed = False
        chStr = '|'
        if npass != '':
            changed = True
            user.set_password(npass)
            chStr = chStr + 'Password|'
        if fname != ofname:
            user.first_name = fname
            changed=True
            chStr = chStr + 'First Name|'
        if lname != olname:
            user.last_name = lname
            changed=True
            chStr = chStr + 'Last Name|'
        if(changed):
            user.save()
        if isUniquePhone:
            user.userextended.phone = phone
            changed=True
            chStr = chStr + 'Phone Number|'
            user.userextended.save()
        #Token automatically generated and to be sent now
        #serializer = TokenSerializer(Token.objects.get(user=user))
        #return Response(serializer.data, status=status.HTTP_200_OK)
        if (not isUniquePhone):
            postlogit(req,"PHONE ALREADY USED")
            return sendMsg("Phone number already in use: " + phone,1)
        if(changed):
            postlogit(req,chStr)
            return sendMsg("User settings updated, changed: " + chStr,1)
        else:
            postlogit(req,"NOTHING CHANGED")
            return sendMsg("User settings not changed!",1)
        
    def get(self, request):
        #return Response("Life is hard. Don't use GET. Tolko POST.", status=status.HTTP_400_BAD_REQUEST)
        return self.do_work(request)

    def post(self, request):
        return self.do_work(request)

def remember_user(request, user, *args, **kwargs):
    """Saves current social-auth status to session."""
    if not user:
        return None
    #print "Here!!!!" + str(user.id)

    request.session["user"] = user.id
    #Also create extended entry, need phone entry!
    try:
        ue = UserExtended.objects.get(uid=user.id)
    except UserExtended.DoesNotExist:
        ue = UserExtended(uid=user) #Empty Phone
        ue.save()

class refreshToken(APIView):
    """
    Logs out user and changes the token so that the old token doesn't work
    """
    def do_work(self,request):
        if not validateAuth(request):
            return sendMsg("Life is hard. Invalid credentials.")
        req = logit(self.__class__.__name__,request)
        t = Token.objects.get(user=request.user)
        #t.key = t.generate_key()
        tp = Token(user=request.user)
        t.delete()
        tp.save()
        res = dict()
        request.session.flush()
        res["message"] = "User logged out: "
        postlogit(req,"LOGGED OUT")
        return Response(res,status=status.HTTP_200_OK)


    def get(self, request):
        #return Response("Life is hard. Don't use GET. Tolko POST.", status=status.HTTP_400_BAD_REQUEST)
        return self.do_work(request)

    def post(self, request):
        return self.do_work(request)


from ios_notifications.models import Device
from ios_notifications.forms import DeviceForm
from ios_notifications.http import JSONResponse

class addDevice(APIView):
    """
        Creates a new device or updates an existing one to `is_active=True`.
        Expects two non-options POST parameters: `token` and `service`.
    """
    def get(self, request):
        return Response("Life is hard. Don't use GET. Tolko POST.", status=status.HTTP_400_BAD_REQUEST)

    def post(self, request):
        if not validateAuth(request):
            return sendMsg("Life is hard. Invalid credentials.")
        req = logit(self.__class__.__name__,request)
        user = request.user
        cstr = checkRequest(['token','service'], request)
        #For android send regId in token field and service will be set to 0
        if(cstr != ""):
            return sendMsg(cstr)
        serv = request.DATA['service']
        token = request.DATA['token']
        if token is not None:
            # Strip out any special characters that may be in the token
            token = re.sub('<|>|\s', '', token)
        
        if(int(serv) == 0): #Android GCM
            ds = GDevice.objects.filter(reg_id =token)
            if(ds.exists()):#Updates device user
                d = ds.get()
                d.is_active=True
                if(d.dev_id != str(user.id)):#device was for a different user, a new user logged in from this device
                    #delete (any) other entry for this user str(user.id)
                    du = GDevice.objects.filter(dev_id = str(user.id))
                    if(du.exists()):
                        du.get().delete()
                    d.dev_id = str(user.id)
                    d.save()
                rvl=d
            else:
                #Check if there's already a device for this user
                du = GDevice.objects.filter(dev_id = str(user.id))
                if(du.exists()):
                    d=du.get()
                    d.reg_id = token
                    d.save() #rewrite the device's reg_id
                else: #create new device
                    d=GDevice(name=user.username,reg_id=token,dev_id=str(user.id))
                    d.is_active=True
                    d.save()
                rvl=d
        else:
            srv = APNService.objects.filter(id = int(serv)).get()
            
            #Need to remove this user from all devices first -- Can't do this! Coz the user might be on multiple ios devices
            #for d in Devices.objects.filter(users=user):
            #    d.users.remove(user)
                
            devices = Device.objects.filter(token=token,service = srv)
            if devices.exists():
                device = devices.get()
                device.is_active = True
                device.users= [user] #remove the previous one
                device.save()
                rvl=device
            else:
                #new Device to be created
                device = Device(token=token,service=srv)
                device.is_active = True
                device.save()
                device.users.add(user)
                device.save()
                rvl=device
        postlogit(req,"Successfully Added Device: " + token + " / Service: " + serv)
        return JSONResponse(rvl, status=200)

class sendPushTest(APIView):
    """
        Sends Test Push message
        service: 0 for android
        msg: message to be sent
        num: number to be sent 
    """
    def get(self, request):
        return Response("Life is hard. Don't use GET. Tolko POST.", status=status.HTTP_400_BAD_REQUEST)

    def post(self, request):
        if not validateAuth(request):
            return sendMsg("Life is hard. Invalid credentials.")
        req = logit(self.__class__.__name__,request)
        user = request.user
        cstr = checkRequest(['msg','service', 'num'], request)
        #For android send regId in token field and service will be set to 0
        if(cstr != ""):
            return sendMsg(cstr)
        serv = request.DATA['service']
        msg = 'bestuff' + request.DATA['msg'] + '#' + request.DATA['num']
        postlogit(req,"msg: " + msg + ",uid: " + str(user.id))
        
        if(int(serv) == 0): #Android GCM
            r = do_push_one_msg_gcm(user,msg)
            return sendMsg("Message sent: " + json.dumps(r),1)
        else:
            r = do_push_one_msg_ios(user,msg)
            return sendMsg("Message sent: " + json.dumps(r),1)
            #return sendMsg("Apple devices not supported")

