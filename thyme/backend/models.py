'''
Copyright 2014, Thyme Labs Inc.

    "Thyme Backend API" is free software: you can redistribute it and/or modify 
    it under the terms of the GNU General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your option) 
    any later version.

    "Thyme Backend API" is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along 
    with "Thyme Backend API".  If not, see<http://www.gnu.org/licenses/>.
'''

# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#     * Rearrange models' order
#     * Make sure each model has one field with primary_key=True
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin.py sqlall [appname]'
# into your database. This is tricky stuff.S

#Need to add for each class so that admin shows it correctly instead of [User Object] etc
#def __unicode__(self):  # Python 3: def __str__(self):
#        return self.question
#or append strings etc .join?
#LOL

from django.db import models
from django.contrib.auth.models import User, Group
import datetime

from django.utils.tzinfo import FixedOffset, LocalTimezone
#auth.User already has id,first_name,last_name,email,dat_joined

DATETIME_FORMAT = '%Y-%m-%dT%H:%M:%SZ' #TODO Tackle TimeZone
ltz = LocalTimezone(datetime.datetime.now())

class UserExtended(models.Model):
    uid = models.OneToOneField(User, db_column='uid',primary_key=True,related_name='userextended') #auth.User has the id primary key
    #fname = models.CharField(max_length=300)
    #lname = models.CharField(max_length=300)
    phone = models.CharField(max_length=60,null=True,blank=True)
    #email = models.CharField(max_length=255, unique=True)
    fbid = models.CharField(max_length=255,null=True,blank=True)
    #joined = models.DateTimeField()

    def __unicode__(self):
        return u'%d:%s' % (self.uid.id,self.uid.username)

    class Meta:
        db_table = u'user_extended'


class What(models.Model): #Tree structure for each activity there might be a parent (unless parentid is -1). Sanctity need to be ensured by hand or tests RTODO
    whatid = models.AutoField(primary_key=True)
    activity = models.CharField(max_length=150,null=True,blank=True)
    parentid =models.IntegerField(default=-1) #default = -1, else another whatid

    def __unicode__(self):
        return u'%d:%s' % (self.whatid, self.activity)

    class Meta:
        db_table = u'what'


class Places(models.Model):
    placeid = models.AutoField(primary_key=True)
    loc_desc = models.TextField() #Can be a custom location description used by a user, created on the fly
    longitude = models.FloatField(null=True,blank=True) #May or may not be present
    lattitude = models.FloatField(null=True,blank=True) #May or may not be present
    address = models.TextField(null=True,blank=True)  #May or may not be present

    def __unicode__(self):
        return u'%d:%s' % (self.placeid, self.loc_desc)

    class Meta:
        db_table = u'places'


#efficiency issue: have repeated schedules for different events or reuse them? For now no reuse!
class Schedules(models.Model):
    scheduleid = models.AutoField(primary_key=True)
    begin = models.DateTimeField()
    end = models.DateTimeField()
    #duration = models.IntegerField() #duration is always end-begin! not needed for now, maybe for efficiency later (indexing on duration)

    def get_begin_corrected(self):
        return self.begin.astimezone(ltz).strftime(DATETIME_FORMAT)

    def get_end_corrected(self):
        return self.end.astimezone(ltz).strftime(DATETIME_FORMAT)


    def __unicode__(self):
        return u'%d:%s-%s' % (self.scheduleid, str(self.begin), str(self.end))

    class Meta:
        db_table = u'schedules'

#Logging Requests
class Requests(models.Model):
    reqid = models.AutoField(primary_key=True)
    when = models.DateTimeField()
    user = models.ForeignKey(UserExtended)
    params = models.TextField()
    func = models.TextField()
    result = models.TextField()
    
    def __unicode__(self):
        return u'%s-%s-%s' % (self.user.uid.username, str(self.when), self.func)

    class Meta:
        db_table = u'requests'



#Needs more efficient representation for recurring events! Or just keep events for the next 30 days, to make things simple
class Events(models.Model):
    eid = models.AutoField(primary_key=True) #incremental ID
    tag = models.CharField(max_length=15) #GC1,IC1 etc or APP for thyme created event
    uid = models.ForeignKey(UserExtended, db_column='uid') #the initiator, maybe allow to change (LATER)
    title = models.CharField(max_length=1500) #added in the beginning and modified on the fly, given by frontend
    whatid = models.ForeignKey(What,blank=True, db_column='whatid') #added for the activity e.g. 3 for "Lunch->Sushi" etc, can be -1 if place selected directly
    scheduleid = models.ForeignKey(Schedules, blank=True, db_column='scheduleid') #By default -1, fixed when event finalizes
    placeid = models.ForeignKey(Places,blank=True, db_column='placeid') #Fixed to begin with
    dead = models.CharField(max_length=3) #By default -1 open event, set to 1 if event canceled, -2 is finalized
    isconf = models.CharField(max_length=1,null=True,blank=True) #Y or N
    confnum = models.CharField(max_length=60,null=True,blank=True) #phone-number
    notes = models.CharField(max_length=3000,null=True,blank=True)
    
    def __unicode__(self):
        if(self.dead == "-2"):
            r1 = u'FINALIZED: %d:%s\'s event:%s | %s | %s-%s' % (self.eid,self.uid.uid.username,self.title,self.scheduleid.begin.astimezone(ltz).strftime("%A %m-%d"),self.scheduleid.begin.astimezone(ltz).strftime("%H:%M"),self.scheduleid.end.astimezone(ltz).strftime("%H:%M"))
        elif(self.dead == "-1"):
            r1 = u'INITIATED: %d:%s\'s event:%s | %s | %s-%s' % (self.eid,self.uid.uid.username,self.title,self.scheduleid.begin.astimezone(ltz).strftime("%A %m-%d"),self.scheduleid.begin.astimezone(ltz).strftime("%H:%M"),self.scheduleid.end.astimezone(ltz).strftime("%H:%M"))
        else:
            r1 = u'DEAD: %d:%s\'s event:%s | %s | %s-%s' % (self.eid,self.uid.uid.username,self.title,self.scheduleid.begin.astimezone(ltz).strftime("%A %m-%d"),self.scheduleid.begin.astimezone(ltz).strftime("%H:%M"),self.scheduleid.end.astimezone(ltz).strftime("%H:%M"))
        return r1
    
    def sent_invites(self):
        if(self.dead == "-2"):
            return "True"
        else:
            return "False"
    class Meta:
        db_table = u'events'


class SchedulesEvents(models.Model): #Not needed for now since scheduleid -> eid mapping is one-one, can help with efficiency later
    scheduleid = models.ForeignKey(Schedules, db_column='scheduleid',primary_key=True)
    eid = models.ForeignKey(Events, db_column='eid')
    class Meta:
        db_table = u'schedules_events'




class RelAttendees(models.Model): #This user has been invited to this event
    eid = models.ForeignKey(Events, db_column='eid',related_name='relattendees')
    uid = models.ForeignKey(UserExtended, db_column='uid')
    relid = models.AutoField(primary_key=True)

    def __unicode__(self):
        return u'%s@%s\'s event:%d/%s' % (self.uid.uid.username,self.eid.uid.uid.username,self.eid.eid,self.eid.title)

    class Meta:
        db_table = u'rel_attendees'

class Attendees(models.Model):#A particular user is going to an event or not or has not responded?
    relid = models.OneToOneField(RelAttendees, db_column='relid',primary_key=True,related_name='attendee')
    accept_status = models.CharField(max_length=3) #Y/N/U

    def __unicode__(self):
        return u'%s->%s' % (self.relid.__unicode__(), self.accept_status)

    class Meta:
        db_table = u'attendees'
                

class RelCancellation(models.Model):
    eid = models.ForeignKey(Events, db_column='eid')
    uid = models.ForeignKey(UserExtended, db_column='uid')
    relid = models.AutoField(primary_key=True)
    class Meta:
        db_table = u'rel_cancellation'
class Cancellation(models.Model):
    relid = models.ForeignKey(RelCancellation, db_column='relid',primary_key=True)
    when = models.DateTimeField()
    msg = models.TextField(null=True,blank=True)
    class Meta:
        db_table = u'cancellation'

class RelConsidersPriority(models.Model):

    uid_l = models.ForeignKey(UserExtended, db_column='uid_l', related_name ='RelConsidersPriority_set_l')
    uid_r = models.ForeignKey(UserExtended, db_column='uid_r', related_name ='RelConsidersPriority_set_r')
    relid = models.AutoField(primary_key=True)
    class Meta:
        db_table = u'rel_considers_priority'
class ConsidersPriority(models.Model):
    relid = models.ForeignKey(RelConsidersPriority, db_column='relid',primary_key=True)
    pr = models.IntegerField()
    class Meta:
        db_table = u'considers_priority'




class RelScheduleAttendees(models.Model): #Not really needed in case there's a one-one correspondence with eid for schedid
#note RTODO: will have to change this in case we optimize and have common scheduleid's IMP
    scheduleid = models.ForeignKey(Schedules, db_column='scheduleid')
    uid = models.ForeignKey(UserExtended, db_column='uid')
    relid = models.AutoField(primary_key=True)
    class Meta:
        db_table = u'rel_schedule_attendees'
class ScheduleAttendees(models.Model): #This user for this schedule said this (STAUS= Y/N/U)
    relid = models.ForeignKey(RelScheduleAttendees, db_column='relid',primary_key=True)
    status = models.CharField(max_length=3)
    class Meta:
        db_table = u'schedule_attendees'

        

class ThirdPartySuggestions(models.Model):
    tpid = models.AutoField(primary_key=True)
    placeid = models.ForeignKey(Places, db_column='placeid')
    link = models.CharField(max_length=1500)
    info = models.TextField(null=True,blank=True)
    class Meta:
        db_table = u'third_party_suggestions'

class RelThirdPartyUser(models.Model):#Entries made here when a user choses a third party "PLACEID" and the status maybe used later, Data oriented for later
    whatid = models.ForeignKey(What, db_column='whatid')
    uid = models.ForeignKey(UserExtended, db_column='uid')
    tpid = models.ForeignKey(ThirdPartySuggestions, db_column='tpid')
    relid = models.AutoField(primary_key=True)
    class Meta:
        db_table = u'rel_third_party_user'
class ThirdPartyUser(models.Model):
    status = models.CharField(max_length=3)
    when = models.DateTimeField()
    relid = models.ForeignKey(RelThirdPartyUser, db_column='relid',primary_key=True)
    class Meta:
        db_table = u'third_party_user'


class AnonFlow(models.Model):
    uid = models.ForeignKey(UserExtended, db_column='uid',primary_key=True)
    anoncode = models.CharField(max_length=1500) #Anonymous login code for thyme.com/anon/<code>
    generated = models.DateTimeField() #Time when anoncode was generated
    active = models.CharField(max_length=1) # Y/N: To be disabled after a while, maybe?
    class Meta:
        db_table = u'anon_flow'


