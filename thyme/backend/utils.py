'''
    Copyright 2014, Thyme Labs Inc.
    
    This file is part of "Thyme Backend API".

    "Thyme Backend API" is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    "Thyme Backend API" is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with "Thyme Backend API".  If not, see <http://www.gnu.org/licenses/>.
'''
__author__ = 'rohitsingh'

ASYNC_SYNC = True

WORKMEETING = 1
BREAKFAST = 2
LUNCH = 3
COFFEE = 4
DINNER = 5
DRINKS = 6
OTHER = 7
widl= ["Work Meeting","Breakfast","Lunch","Coffee","Dinner","Drinks","Event"]

CREATENEWUSERS = False

def wid2str(id):
    if(id <1 or id > 7): return "Event"
    return widl[id-1]

DBG = True
APPERY_MASTER_KEY="4cb66249-b503-4afe-b3cc-f0aeebe13eda"
from time import strptime
#DATETIME_FORMAT = '%Y-%m-%dT%H:%M:%SZ' #TODO Tackle TimeZone
#imported in models 
from django.core.mail import send_mail
from math import fabs
from thyme.social_settings import *
from django.shortcuts import render
import iso8601
import httplib2
import requests
import json
import re
import pytz
from django.dispatch.dispatcher import receiver
from rest_framework.authtoken.models import Token
from django.db.models.signals import post_save
from apiclient.discovery import build
from oauth2client.file import Storage
from oauth2client.client import OAuth2Credentials
from django.http import Http404
from BitVector import *
from urllib import urlencode
from urllib2 import Request
from django.utils import simplejson
from pytz import utc
from social_auth.backends.google import GoogleOAuth2
from social_auth.utils import dsa_urlopen
from django.contrib.auth.models import User
import datetime
from datetime import timedelta
from models import *
from django.db.models import Q
from rest_framework.response import Response
from rest_framework import status
from ios_notifications.models import *
from gcm.models import Device as GDevice



def login(request):
    return render(request, 'login.html')


def validateAuth(request):
    if request.auth is None:
        return False
    else:
        return True

GDATE_FORMAT = '%Y-%m-%dT%H:%M:%S.000Z'
GDATE_FORMAT_BEZ_TZ = '%Y-%m-%dT%H:%M:%S.000'
def format_datetime(date):
    """
    A utility method that converts the datetime to UTC serialized
    for Google Calendar.
    Ignores tzinfo of given date and replaces it with local TimeZone
    Then converts it to GDATE_FORMAT format
    """
    local = date.replace(tzinfo=ltz)
    #return local.astimezone(FixedOffset(0)).strftime(GDATE_FORMAT)
    strlocal = local.strftime(GDATE_FORMAT_BEZ_TZ)
    localoffset = ltz.utcoffset(datetime.datetime.now())
    tzv = (localoffset.days * 86400 + localoffset.seconds) / 1800
    at = int(fabs(tzv))
    if(tzv < 0):
        sgn="-"
    else:
        sgn="+"
    if(at < 20):
        d1 = "0"
    else:
        d1=""
    if(at % 2 == 0):
        endd = "00"
    else:
        endd = "30"
    strlocal = strlocal + sgn + d1 + str(int(at/2)) + endd
    return strlocal

def reverse_format_datetime(gdate):
    """
    Assume given gdate in EDT in GDATE_FORMAT
    No need to convert to local TimeZone
    """
    gdate = iso8601.parse_date(gdate) #strptime(gdate,GDATE_FORMAT)
    #gdate = gdate.replace(tzinfo=utc)
    #ldate = gdate.astimezone(LocalTimezone(datetime.datetime.now()))
    return gdate.astimezone(pytz.utc)#.replace(tzinfo=utc)


TOKEN_URI = 'https://accounts.google.com/o/oauth2/token'


def refresh_token_function(user_social_auth):
    client_id, client_secret = GoogleOAuth2.get_key_and_secret()
    params = {'grant_type': 'refresh_token',
              'client_id': client_id,
              'client_secret': client_secret,
              'refresh_token': user_social_auth.extra_data['refresh_token']}
    request = Request(TOKEN_URI, data=urlencode(params), headers={
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json'
    })

    try:
        response = simplejson.loads(dsa_urlopen(request).read())
    except (ValueError, KeyError):
        # Error at response['error'] with possible description at
        # response['error_description']
        pass
    else:
        # Keys in response are: access_token, token_type, expires_in, id_token
        user_social_auth.extra_data['access_token'] = response['access_token']
        user_social_auth.save()

def getCalService(uid):
    u = User.objects.get(id=uid)
    try:
        g = u.social_auth.get(provider='google-oauth2')
    except:
        return None

    storage = Storage(GOOGLE_API_CREDENTIAL_PATH + str(u.id)  + '.dat')
    credentials = storage.get()
    if credentials is None or credentials.invalid == True:
        token_uri = "https://accounts.google.com/o/oauth2/token"
        revoke_uri = "https://accounts.google.com/o/oauth2/revoke"
        if not ('refresh_token' in g.extra_data):
            return None
        refresh_token_function(g)
        token_expiry = datetime.datetime.utcnow() + datetime.timedelta(seconds=int(g.extra_data['expires']))
        credentials = OAuth2Credentials(g.extra_data['access_token'], GOOGLE_OAUTH2_CLIENT_ID,
                               GOOGLE_OAUTH2_CLIENT_SECRET, g.extra_data['refresh_token'], token_expiry,
                               token_uri, "Thyme/1",
                               revoke_uri=revoke_uri,
                               id_token=None,
                               token_response=None)
        storage.put(credentials)
    http = httplib2.Http()
    http = credentials.authorize(http)

    # Build a service object for interacting with the API. Visit
    # the Google APIs Console
    # to get a developerKey for your own application.
    service = build(serviceName='calendar', version='v3', http=http,
           developerKey='AIzaSyCkzBe4CFehrFsXoZqZVKjj_aLu2SrFmeA')
    return service

#A function that given a cal-service for a user can get the calendar entries and the busyness vector
def getCalBitVector(service,date,req): #service should be for that user
    date.replace(hour=0,minute=0,second=1)
    bv = BitVector(intVal=0, size=96)
    if(service is None):
        return bv
    start_min = format_datetime(date) #format_datetime(date)
    
    start_max = format_datetime(date + timedelta(hours=24)) #format_datetime(date + timedelta(hours=24))
    #req.params = req.params + "|min=" + str(start_min) + " - " + str(start_max) + "|" 
    
    #get all owned calendars
#     page_token = None
#     idl = []
#     while True:
#         calendar_list = service.calendarList().list(minAccessRole="owner",pageToken=page_token).execute()
#         for calendar_list_entry in calendar_list['items']:
#             idl.append(calendar_list_entry['id'])
#         page_token = calendar_list.get('nextPageToken')
#         if not page_token:
#             break
#     #idl has all ids of the owned calendars
    idl = ['primary']
    for idc in idl:
        page_token=None
        while True:
            #events = service.events().list(calendarId='primary', pageToken=page_token, timeMin=start_min,timeMax = start_max,timeZone=ltz).execute()
            events = service.events().list(calendarId=idc, pageToken=page_token, timeMin=start_min,timeMax = start_max,timeZone=ltz).execute()
            for event in events['items']:
                if ('start' in event and 'dateTime' in event['start']):
                    dstime = reverse_format_datetime(event['start']['dateTime']) #In local timezone format
                else:
                    continue
                if ('end' in event and 'dateTime' in event['end']):
                    detime = reverse_format_datetime(event['end']['dateTime'])
                else:
                    continue
                #req.params = req.params + "|event=" + str(dstime) + " - " + event['summary'] +"|" 
                (x,y)=getBitsRange(dstime,detime)
                bv[x:(y+1)] = bv[x:(y+1)].reset(1)
            page_token = events.get('nextPageToken')
            if not page_token:
                break
    return bv

def getCalIds(service):
    page_token = None
    idl = []
    while True:
        calendar_list = service.calendarList().list(minAccessRole="owner",pageToken=page_token,showHidden=True).execute()
        for calendar_list_entry in calendar_list['items']:
            if((not ("hidden" in calendar_list_entry)) or calendar_list_entry['hidden'] == False):
                idl.append(calendar_list_entry['id'])
        page_token = calendar_list.get('nextPageToken')
        if not page_token:
            break
    #idl has all ids of the owned calendars
    return idl
def updateCalBusyAllGCalsOwned(user,service,dateBegin,dateEnd,DEL):
    if(service is None):
        return "-1"
    #get all owned calendars
    idl = getCalIds(service)
    if(DEL):
        allEvsUser = Events.objects.filter(tag="GC",uid=user.userextended)
        allEvsUser.delete()
    fq = dict()
    fq["timeMin"] = format_datetime(dateBegin)
    fq["timeMax"] = format_datetime(dateEnd)
    lst = []
    for id in idl:
        d = dict()
        d["id"] = id
        lst.append(d) 
    fq["items"] = lst
    qres = service.freebusy().query(body=fq).execute()
    res =0
    if("calendars" in qres):
        for id,rhs in qres["calendars"].iteritems():
                if("busy" in rhs):
                    for bev in rhs["busy"]:
                        dstime = reverse_format_datetime(bev["start"])
                        detime = reverse_format_datetime(bev["end"]) 
                        if(not DEL):
                            foundEvent = False
                            for ev in Events.objects.filter(scheduleid__begin=dstime,scheduleid__end=detime):
                                foundEvent = True
                                break
                            if(foundEvent):
                                continue
                        if(dstime.day != detime.day): #don't consider day long events or events spanning to two days
                            #But then what if it was a composite? we lose smaller events TODO
                            continue
                        sch = Schedules(begin=dstime,end=detime)
                        sch.save()
                        wh = What.objects.get(whatid=OTHER)
                        loc="GCAL_EMPTY"
                        pl = addNewPlaceIfNotExists(-1,loc)
                        e = Events(tag="GC",title="BUSY",uid=user.userextended,scheduleid=sch,whatid=wh,placeid=pl)
                        res = res + 1
                        e.save()
    return "Added Events: " + str(res)

def updateCalEntriesAllGCalsOwned(user,service,dateBegin,dateEnd,DEL):
    if(service is None):
        return -1
    #get all owned calendars
    idl = getCalIds(service)
    if(DEL):
            allEvsUser = Events.objects.filter(tag="GC",uid=user.userextended,scheduleid__begin__gt=dateBegin,scheduleid__end__lt=dateEnd)
            allEvsUser.delete()
    #curDate = dateBegin
    
    #while(curDate <= dateEnd):
        #secDate = curDate + timedelta(days=1)
    start_min = format_datetime(dateBegin) #format_datetime(date)
    start_max = format_datetime(dateEnd) #format_datetime(date + timedelta(hours=24))
    res = 0

    #need to delete all GC events for this user in the DB
   
    if (not idl):
        idl=['primary']
    for idc in idl:
        page_token=None
        while True:
            events = service.events().list(calendarId=idc, pageToken=page_token, timeMin=start_min,timeMax = start_max,timeZone=ltz,singleEvents=True).execute()
            for event in events['items']:
                if('summary' in event):
                    title = event['summary']
                else:
                    title = '#No Title'
                if ('start' in event and 'dateTime' in event['start']):
                    dstime = reverse_format_datetime(event['start']['dateTime']) #In local timezone format
                else:
                    continue
                if ('end' in event and 'dateTime' in event['end']):
                    detime = reverse_format_datetime(event['end']['dateTime'])
                else:
                    continue
                
                if(not DEL):
                    foundEvent = False
                    for ev in Events.objects.filter(scheduleid__begin=dstime,scheduleid__end=detime):
                        if(ev.title.find(title) != -1):
                            #Info exits, just ignore
                            foundEvent = True
                            break
                        else:
                            ev.title = ev.title + "," + title
                            ev.save()
                            foundEvent = True
                            break
        
                    if(foundEvent):
                        continue
                    
                sch = Schedules(begin=dstime,end=detime)
                sch.save()
                wh = What.objects.get(whatid=OTHER)
                if('location' in event):
                    loc = event['location']
                else:
                    loc="GCAL_EMPTY"
                if(loc == ''):
                    loc="GCAL_EMPTY"
                pl = addNewPlaceIfNotExists(-1,loc)
                e = Events(tag="GC",title=unicode(title),uid=user.userextended,scheduleid=sch,whatid=wh,placeid=pl)
                res = res + 1
                #TODO Add Attendees to the event
                e.save()
            page_token = events.get('nextPageToken')
            if not page_token:
                break
            #curDate = secDate
    return "Added Events: " + str(res)


def updateCalEntries(user,service,dateBegin,dateEnd):
    if(service is None):
        return -1
    start_min = format_datetime(dateBegin) #format_datetime(date)
    start_max = format_datetime(dateEnd) #format_datetime(date + timedelta(hours=24))
    page_token=None
    res = 0
    while True:
        events = service.events().list(calendarId='primary', pageToken=page_token, timeMin=start_min,timeMax = start_max,timeZone=ltz,singleEvents=True ).execute()
        for event in events['items']:
            if('summary' in event):
                title = event['summary']
            else:
                title = '#No Title'
            if (not ('dateTime' in event['start'])) or (not ('dateTime' in event['end'])):
                continue
            dstime = reverse_format_datetime(event['start']['dateTime']) #In local timezone format
            detime = reverse_format_datetime(event['end']['dateTime'])
            #check if schedules exist
            foundEvent = False
            for ev in Events.objects.filter(scheduleid__begin=dstime,scheduleid__end=detime):
                if(ev.title.find(title) != -1):
                    #Info exits, just ignore
                    foundEvent = True
                    break
                else:
                    ev.title = ev.title + "," + title
                    ev.save()
                    foundEvent = True
                    break

            if(foundEvent):
                continue

            sch = Schedules(begin=dstime,end=detime)
            sch.save()
            wh = What.objects.get(whatid=OTHER)
            if('location' in event):
                loc = event['location']
            else:
                loc="GCAL_EMPTY"
            if(loc == ''):
                loc="GCAL_EMPTY"
            pl = addNewPlaceIfNotExists(-1,loc)
            e = Events(tag="GC",title=title,uid=user.userextended,scheduleid=sch,whatid=wh,placeid=pl)
            res = res + 1
            #TODO Add Attendees to the event
            e.save()
        page_token = events.get('nextPageToken')
        if not page_token:
            break
    return "Added Events: " + str(res)


def addEvent(ev,service,user,ulist):
    gev = dict()
    gev['summary']=ev.title
    gev['location'] = ev.placeid.loc_desc
    gev['start'] = dict()
    gev['start']['dateTime'] = format_datetime(ev.scheduleid.begin)
    gev['end'] = dict()
    gev['end']['dateTime'] = format_datetime(ev.scheduleid.end)
    gev['attendees'] = []
    for u in ulist:
        if(u != user):
            att = dict()
            att['email'] = u.email
            gev['attendees'].append(att)
    created_event = service.events().insert(calendarId='primary', body=gev).execute()
    return created_event['id']
def getNumBits(hour,minute):
    return (hour*4 + int(minute/15))

def getBitsRange(begin,end):
    #both datetime
    (x,y) = ((begin.hour * 4 + (begin.minute/15)),(end.hour * 4 + (end.minute/15)))
    if(y < x):
        y=95;
    return (x,y)

def transformDT(date_std):
    return datetime.datetime(year=date_std.tm_year,month=date_std.tm_mon,day=date_std.tm_mday,hour=date_std.tm_hour,minute=date_std.tm_min,second=date_std.tm_sec,tzinfo=ltz)

def getUserBitVector(uid,date1):
    
    #Everything in the database is stored in GMT format so we need to modify date1 to help find the right events
    date1 = date1.replace(hour=0,minute=0,second=0,tzinfo=ltz)
    date2 = date1 + timedelta(hours=23,minutes=59)
    #get all events on this date for this user
    bv = BitVector(intVal=0, size=96)
    #All events which either belong to them or for which they have said yes
    for e in Events.objects.filter(Q(uid=uid) & Q(scheduleid__begin__gte=date1) & Q(scheduleid__end__lte=date2)):
        #all events in this range
        if(not (e.scheduleid is None)):
            sc = e.scheduleid
            begtime = sc.begin.astimezone(ltz)
            endtime = sc.end.astimezone(ltz)
            if(begtime.day != endtime.day):
                #ignore this schedule
                (x,y)=getBitsRange(begtime,endtime.replace(day=begtime.day,hour=23,minute=45,second=0))
            else:
                (x,y)=getBitsRange(begtime,endtime)
            bv[x:(y+1)] = bv[x:(y+1)].reset(1)
    return bv

def getCustomBitVector(date1,evLst):
    date2 = date1 + timedelta(hours=23,minutes=59)
    #get all events on this date for this user
    bv = BitVector(intVal=0, size=96)
    #All events which either belong to them or for which they have said yes
    for (dbeg,dend) in evLst:
        if(dbeg.day == date1.day and dbeg.month == date1.month and dbeg.year == date1.year):
            if(not (dend.day == date1.day and dend.month == date1.month and dend.year == date1.year)):
                (x,y)=getBitsRange(dbeg,dend.replace(day=dbeg.day,hour=23,minute=45,second=0))
            else:
                (x,y)=getBitsRange(dbeg,dend)
            bv[x:(y+1)] = bv[x:(y+1)].reset(1)
        elif(dend.day == date1.day and dend.month == date1.month and dend.year == date1.year):
            if(not (dbeg.day == date1.day and dbeg.month == date1.month and dbeg.year == date1.year)):
                (x,y)=getBitsRange(dbeg.replace(day=dbeg.day,hour=0,minute=15,second=0),dend)
            else:
                (x,y)=getBitsRange(dbeg,dend)
            bv[x:(y+1)] = bv[x:(y+1)].reset(1)
    return bv

def findAllPossibleSlots(bv,bitsReq):
    #returns a list of (x,l), from position x, l slots are available
    import re
    n='0'
    return [m.start() for m in re.finditer('(?=' + n.zfill(bitsReq) + ')', str(bv))]

from math import ceil

def takespread(sequence, num):
    ls = (len(sequence))
    f = lambda m, n: [i*n//m + n//(2*m) for i in range(m)]
    return [sequence[i] for i in f(num,ls)]


def getEventIfExists(eid):
    try:
        return (True,Events.objects.get(eid=eid))
    except Events.DoesNotExist:
        return (False,None)



def sanctify(phone):
    #remove everything except numbers
    rs = re.sub("[^0-9]", "", phone)
    if(len(rs) > 10):
        return rs[-10:]
    else:
        return rs
def updateUIDs(otherUsers):

    for userInfo in otherUsers.values():
        #if UID absent then find it from DB using phone/email/fbid
        if('phone' in userInfo):
            userInfo['phone'] = sanctify(userInfo['phone']) #US 10 Digits
        if( (not ('uid' in userInfo)) or (userInfo['uid'] <= 0)):
            #Either email or phone should be set
            #if not ( ( ('email' in userInfo) and (userInfo['email'] != '')) or ( ('phone' in userInfo) and (userInfo['phone'] != ''))):
            emailExists = ( ('email' in userInfo) and (userInfo['email'] != ''))
            phoneExists = ( ('phone' in userInfo) and (userInfo['phone'] != ''))
            if((not phoneExists) and (not emailExists)):
                return "Invalid or empty phone/email entries in userInfo"
            phoneValid=phoneExists
            emailValid=emailExists
            if(phoneExists):
                try:
                    tusr = UserExtended.objects.get(phone=userInfo['phone'])
                    userInfo['user'] = tusr
                    userInfo['uid'] = tusr.uid.id
                    phoneValid = True
                except UserExtended.DoesNotExist:
                    phoneValid = False
                    
            if((not phoneValid) and emailExists):
                try:
                    tusr = User.objects.get(email=userInfo['email'])
                    userInfo['user'] = tusr.userextended
                    userInfo['uid'] = tusr.id
                    emailValid = True
                except User.DoesNotExist:
                    emailValid = False
            
            if((not phoneValid) and (not emailValid)):
                #Create New User since phone/email exists and they are both a unique pair or singleton
                unm = ""
                phnum = ""
                eml = ""
                if(phoneExists):
                    phnum = sanctify(userInfo['phone'])
                    unm = phnum
                if(emailExists):
                    eml = userInfo['email']
                    unm = eml
                u = User.objects.create_user(username=unm,email=eml,password="")
                u.password=""
                u.save()
                ue = UserExtended(uid=u,phone=phnum) 
                ue.save()
                userInfo['user'] = ue
                userInfo['uid'] = u.id
        else:
            userInfo['user'] = UserExtended.objects.get(uid=userInfo['uid'])

    #Now we have all uid's needed to make the entry
    for userInfo in otherUsers.values():
        if not ('uid' in userInfo):
            return "uids still not in userInfo, its abominable!"
    return ""

def checkDevice(user):
    #For this user there should a device registered for Push
    ds = GDevice.objects.filter(dev_id = user.id)
    if(ds.exists()):
        return True
    srv = APNService.objects.filter(id = 1).get()
    ds = Device.objects.filter(users=user,service=srv)
    if(ds.exists()):
        return True
    return False

def findUsers(otherUsers): #Should have a valid 
    dkeys = []
    for k,userInfo in otherUsers.iteritems():
        #if UID absent then find it from DB using phone/email/fbid
        if('phone' in userInfo):
            userInfo['phone'] = sanctify(userInfo['phone']) #US 10 Digits
        if( (not ('uid' in userInfo)) or (userInfo['uid'] <= 0)):
            #Either email or phone should be set
            #if not ( ( ('email' in userInfo) and (userInfo['email'] != '')) or ( ('phone' in userInfo) and (userInfo['phone'] != ''))):
            emailExists = ( ('email' in userInfo) and (userInfo['email'] != ''))
            phoneExists = ( ('phone' in userInfo) and (userInfo['phone'] != ''))
            
            phoneValid=phoneExists
            emailValid=emailExists
            if((not phoneExists) and (not emailExists)):
                return "Invalid or empty phone/email entries in userInfo"
            if(phoneExists):
                try:
                    tusr = UserExtended.objects.get(phone=userInfo['phone'])
                    if(checkDevice(tusr.uid)):
                        #userInfo['user'] = tusr
                        userInfo['uid'] = tusr.uid.id
                        userInfo['phone'] = tusr.phone
                        userInfo['email'] = tusr.uid.email
                        phoneValid = True
                    else:
                        phoneValid = False
                except UserExtended.DoesNotExist:
                    phoneValid = False
                    
            if((not phoneValid) and emailExists):
                try:
                    tusr = User.objects.get(email=userInfo['email'])
                    #userInfo['user'] = tusr.userextended
                    if(checkDevice(tusr)):
                        userInfo['uid'] = tusr.id
                        userInfo['phone'] = tusr.userextended.phone
                        userInfo['email'] = tusr.email
                        emailValid = True
                    else:
                        emailValid = False
                except User.DoesNotExist:
                    emailValid = False
        else:
            userInfo['user'] = UserExtended.objects.get(uid=userInfo['uid'])
        if(not ('uid' in userInfo)):
            dkeys.append(k)
    for k in dkeys:
        del otherUsers[k]
    return otherUsers
    #Now we have all uid's needed to make the entry

def addNewPlaceIfNotExists(placeId,locDesc):
    #Add Entry in PLACES if PLACEID == -1 and LOC_DESC is non-empty, so that it comes in search next time. Avoid Duplicaction here. Otherwise no Entry if PLACEID is set or can find exact loc_desc string.
    if (not (placeId > 0 or locDesc != '')):
        locDesc = "TBD"

    if(placeId < 0):
        #first check if there's any existing entry with same description
        try:
            return Places.objects.get(loc_desc=locDesc)
        except Places.DoesNotExist:
            #add a new place in db
            p = Places(loc_desc=locDesc) #long,latt, address are empty for now
            p.save()
            return p #id is set after save!
    else:
        return Places.objects.get(placeid=placeId)

def newSchedule(selectedBegin,selectedEnd):
    #TODO What about using same schedules? Later
    schds = list(Schedules.objects.filter(begin=selectedBegin,end=selectedEnd)[:1])
    if(schds):
        schd = schds[0]
    else:
        schd = Schedules(begin=selectedBegin,end=selectedEnd)
        schd.save()
    return schd

def addOrUpdateAttendee(e,user,status):
    try:
        ra = RelAttendees.objects.get(eid=e,uid=user)
        try:
            att = Attendees.objects.get(relid=ra)
            att.accept_status = status
            att.save()
        except Attendees.DoesNotexist:
            att = Attendees(relid=ra,accept_status=status)
            att.save()
    except RelAttendees.DoesNotExist:
        ra = RelAttendees(eid=e,uid=user);
        ra.save()
        att = Attendees(relid=ra,accept_status=status)
        att.save()

def getAttendees(event):
    ulist =[event.uid.uid]
    for rela in RelAttendees.objects.filter(eid=event):
        status = Attendees.objects.get(relid=rela).accept_status
        if(status == "Y"):
            ulist.append(rela.uid.uid)
    return list(set(ulist))


def addICEvents(evLst,userInit):
    #Add all events in the evLst
    what = What.objects.get(whatid=1)
    
    allEvsUser = Events.objects.filter(tag="ICL",uid=userInit)
    allEvsUser.delete()
    
    k=0
    for (dbeg,dend) in evLst:
        #evs = Events.objects.filter(scheduleid__begin=dbeg,scheduleid__end=dend)
        #if(not evs.exists()):
        place = addNewPlaceIfNotExists(-1,"TBD")
        #Add an entry in SCHEDULES table {begin, end} [date is assumed in begin/end]... based on SelectedTime and also relate it  to this event in Schedules_Event Table (RTODO later make efficient with schedule search)
        schd = newSchedule(dbeg,dend)
        e = Events(tag="ICL",uid=userInit,title="Busy",whatid=what,scheduleid=schd,placeid=place,dead="-1")
        e.save()
        k=k+1
    return k

def addICEventsNoDel(evLst,userInit):
    #Add all events in the evLst
    what = What.objects.get(whatid=1)
    k=0
    for (dbeg,dend) in evLst:
        evs = Events.objects.filter(scheduleid__begin=dbeg,scheduleid__end=dend)
        if(not evs.exists()):
            place = addNewPlaceIfNotExists(-1,"TBD")
            #Add an entry in SCHEDULES table {begin, end} [date is assumed in begin/end]... based on SelectedTime and also relate it  to this event in Schedules_Event Table (RTODO later make efficient with schedule search)
            schd = newSchedule(dbeg,dend)
            e = Events(tag="ICL",uid=userInit,title="Busy",whatid=what,scheduleid=schd,placeid=place,dead="-1")
            e.save()
            k=k+1
    return k


def getFreeThymes(uidList,onDate,duration, whatId,numThymes,calServ,CALACT,evLst,req):
    bv = BitVector(intVal=0, size=96)
    dtoday = datetime.datetime.now()
#    itsToday = True
    if(onDate.day == dtoday.day and onDate.month == dtoday.month and onDate.year == dtoday.year):
        #we are searching for today
#        itsToday= True 
        if(dtoday.hour < 23):
            dtoday.replace(hour=(dtoday.hour+1))
        elif(dtoday.hour == 23):
            dtoday.replace(minute=59)
        y=getNumBits(dtoday.hour,dtoday.minute)
        #req.params = req.params + "|y=" + str(y) + "|"

        bv[0:(y+1)] = bv[0:(y+1)].reset(1)
    
    bvcustom = getCustomBitVector(onDate,evLst)
    bv = bv | bvcustom        
#    if(itsToday): 
#        req.params = req.params + "|bvcustom=" + str(bvcustom) + "|"
    for uid in uidList:
        bvtemp = getUserBitVector(uid,onDate)
        if(CALACT and (not ASYNC_SYNC)):
            bvcaltemp = getCalBitVector(calServ[str(uid)],onDate,req)
        bv = bv | bvtemp
#        if(itsToday): 
#            req.params = req.params + "|uid=" + str(uid) + "|" + "|onDate=" + str(onDate) + "|" + "|bvtemp=" + str(bvtemp) + "|"
        if(CALACT and (not ASYNC_SYNC)):
            bv = bv | bvcaltemp
#            if(itsToday): 
#                req.params = req.params + "|bvcaltemp=" + str(bvcaltemp) + "|"

    bitsReq = duration/15

    #we need bitsReq bits of consecutive 0s
    #Based on whatid, prioritize?
    #Find all blocks of bitsReq size or more
    lstSlots = findAllPossibleSlots(bv,bitsReq)
#    if(itsToday): 
#        req.params = req.params + "|" + str(bv) + "|"

    #req.params = req.params + "|" + str(bv) + "|"
    #reorder based on heuristics and choose top
    #For now just based on what constrain and choose equidistant numThymes
    beg = 0 #hour*4
    end = 95
    if(whatId == BREAKFAST): #7-11
        beg = 28
        end = 44
    elif (whatId == LUNCH): #12-15
        beg = 48
        end = 60
    elif (whatId == DINNER): #18-22
        beg = 72
        end = 88
    elif (whatId == WORKMEETING): #9-17
        beg = 32
        end = 68
    lstSmall = [i for i in lstSlots if (i >= beg and i <= end)]
    #Equidistant numThymes slots
    
    #Try to see if we can find slots beginning at even hours
    lstWholeHours = [ i for i in lstSmall if (i%2 == 0) ] 
    szWhole = len(lstWholeHours)
    
    sz = len(lstSmall)
    if(sz <= numThymes):
        return lstSmall
    else:
        if(DBG):
            print "lstSmall:"
            print lstSmall
        if(szWhole <= numThymes):
            ltemp = takespread(list(set(lstSmall) - set(lstWholeHours)),numThymes-szWhole)
            return lstWholeHours + ltemp 
        else:
            ltemp = takespread(lstWholeHours,numThymes)
            if(DBG):
                print "ltemp"
                print ltemp
                return ltemp

#Random change
@receiver(post_save, sender=User)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        if(not Token.objects.filter(user=instance)):
            Token.objects.create(user=instance)

def syncGCal(user,beginDate,endDate):
    service = getCalService(user.id)
    if (service is None):
        return "User calendar not shared"
    return updateCalEntriesAllGCalsOwned(user,service,beginDate,endDate,True)
    #return updateCalBusyAllGCalsOwned(user,service,beginDate,endDate,True)

def syncGCalNoDel(user,beginDate,endDate):
    service = getCalService(user.id)
    if (service is None):
        return "User calendar not shared"
    return updateCalEntriesAllGCalsOwned(user,service,beginDate,endDate,False)
    #return updateCalBusyAllGCalsOwned(user,service,beginDate,endDate,False)

def sendMsg(str,stat=None):
    d = dict()
    if(stat==1):
        d['message'] = str
        return Response(d,status=status.HTTP_200_OK)
    else:
        d['error'] = str
        return Response(d,status=status.HTTP_200_OK)#CHANGED 15 JAN message vs error parameter in data

def send_gcm(api_key, regs_id, data, collapse_key=None):
    """
    Send a GCM message for one or more devices, using json data
    api_key: The API_KEY from your console (https://code.google.com/apis/console, locate Key for Server Apps in
        Google Cloud Messaging for Android)
    regs_id: A list with the devices which will be receiving a message
    data: The dict data which will be send
    collapse_key: A string to group messages, look at the documentation about it:
        http://developer.android.com/google/gcm/gcm.html#request
    """
    values = {
        'registration_ids': regs_id,
        'collapse_key': collapse_key,
        'time_to_live': 3600,
        'delay_while_idle': True,
        'data': data
    }


    headers = {
        'UserAgent': "GCM-Server",
        'Content-Type': 'application/json',
        'Authorization': 'key=' + api_key,
    }

    response = requests.post(url="https://android.googleapis.com/gcm/send",data=json.dumps(values), headers=headers)
    r = json.loads(response.content)
    return r

def sendGCMPush(regs_id,message):
    #regs_id = list()
    #for device in devices_a :
    #    regs_id.append(device.token_string)                    


    message = json.dumps(message)
    values = {
        'registration_ids': regs_id,
        'collapse_key': "message" ,
        'data': {"message":str(msg.message)}
    }   

    headers = {
        'UserAgent': "GCM-Server",
        'Content-Type': 'application/json',
        'Authorization': 'key=' + settings.GCM_APIKEY,
    }

    response = requests.post(url="https://android.googleapis.com/gcm/send",data=json.dumps(values), headers=headers)

    r = json.loads(response.content)
    msg.nbr_android_recieved = r["success"]

def checkRequest(check_params,request):
    for s in check_params:
        if not (s in request.DATA):
            return ("Missing parameter in request.DATA: " + s )
        if request.DATA[s] is None:
            return ("Null value for parameter in request.DATA: " + s )
    return ""

def send_push(d,msg):#device andmessage given
    headers={'X-Appery-Push-Master-Key':APPERY_MASTER_KEY, 'Content-Type': 'application/json'}
    payload = {"channels":[1608], "message": msg, "badge":"0", "deviceID":d.reg_id}
    url = 'https://api.appery.io/rest/push/msg/key'
    requests.post(url, data=json.dumps(payload), headers=headers)


def get_name(u):
    if(u.first_name != ''):
        return u.first_name + " " + u.last_name;
    else:
        return u.username

def sendEmail(u,subject,msg):
    send_mail(subject, msg, "noreply@thyme.io", [u.email])
    return


#Logging Function
def logit(fname,request):
    #Need to be authenticated user
    if(request.user):
        r = Requests(when=datetime.datetime.now(),user=request.user.userextended,params=json.dumps(request.DATA),func=fname,result="INCOMPLETE")
        r.save()
        return r

#Logging Function
def logitreq(fname,request):
    #Doesn't need to be authenticated user
    r = Requests(when=datetime.datetime.now(),user=User.objects.get(id=1).userextended,params=json.dumps(request.DATA),func=fname,result="INCOMPLETE")
    r.save()
    return r

    
#Logging result of a function
def postlogit(req,res):
    #Need to be authenticated user
    if(isinstance(res,basestring)):
        req.result = res
    else:
        req.result = json.dumps(res)
    req.save()
    
def toUTC(dt):
    return dt.replace(tzinfo=ltz).astimezone(pytz.utc)
