'''
Copyright 2014, Thyme Labs Inc.

    "Thyme Backend API" is free software: you can redistribute it and/or modify it 
    under the terms of the GNU General Public License as published by the Free 
    Software Foundation, either version 3 of the License, or (at your option) 
    any later version.

    "Thyme Backend API" is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License 
    for more details.

    You should have received a copy of the GNU General Public License along 
    with "Thyme Backend API".  If not, see<http://www.gnu.org/licenses/>.
'''

from django.forms import widgets
from rest_framework import serializers
from models import *
from django.contrib.auth.models import User
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.dispatch import receiver
from django.db.models.signals import post_save
from rest_framework.authtoken.models import Token
#def validateEmail( email ):
#    from django.core.validators import validate_email
#    from django.core.exceptions import ValidationError
#    try:
#        validate_email( email )
#        return True
#    except ValidationError:
#        return False




#whenever a User instantiation is saved, it will have a token created for itself
@receiver(post_save, sender=User) #execute after save() function has been called by an object of the model class User
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)



class authUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'email','username','first_name','last_name')
        #depth = 0


class UserSerializer(serializers.ModelSerializer):
    uid = authUserSerializer()
    class Meta:
        model = UserExtended
        fields = ('uid', 'phone')
        #depth = 1


class scheduleSerializer(serializers.ModelSerializer):
    beginc = serializers.CharField(source='get_begin_corrected',read_only=True)
    endc = serializers.CharField(source='get_end_corrected',read_only=True)

    class Meta:
        model = Schedules
        exclude = ('begin','end')

class whatSerializer(serializers.ModelSerializer):
    class Meta:
        model = What

class placeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Places

class AttendeesSerializer(serializers.ModelSerializer):

    class Meta:
        model = Attendees
        fields = ('accept_status',)

class relAttendeesSerializer(serializers.ModelSerializer):
    uid = UserSerializer()
    attendee = AttendeesSerializer()
    class Meta:
        model = RelAttendees
        fields = ('uid','attendee')
        depth = 1

class EventLongSerializer(serializers.ModelSerializer):
    scheduleid = scheduleSerializer()
    whatid = whatSerializer()
    placeid = placeSerializer()
    uid = UserSerializer()
    sentInvites = serializers.CharField(source='sent_invites',read_only=True)
    relattendees = relAttendeesSerializer()
    #relattendees = serializers.RelatedField(many=True)
    class Meta:
        model = Events
        fields = ('eid','tag','uid','title','scheduleid','whatid','placeid','dead','relattendees','sentInvites','isconf','confnum','notes')
        depth = 2


class EventShortSerializer(serializers.ModelSerializer):
    scheduleid = scheduleSerializer()
    whatid = whatSerializer()
    sentInvites = serializers.CharField(source='sent_invites',read_only=True)
    class Meta:
        model = Events
        fields = ('eid','uid','title','scheduleid','whatid','sentInvites')
        depth = 1

class TokenSerializer(serializers.ModelSerializer):
    class Meta:
        model = Token
        fields =('key', 'user')

