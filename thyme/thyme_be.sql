-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 10, 2013 at 07:17 PM
-- Server version: 5.5.29
-- PHP Version: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `thyme_be`
--

-- --------------------------------------------------------

--
-- Table structure for table `anon_flow`
--

CREATE TABLE IF NOT EXISTS `anon_flow` (
  `uid` int(11) NOT NULL,
  `anoncode` varchar(1500) NOT NULL,
  `generated` datetime NOT NULL,
  `active` varchar(1) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `attendees`
--

CREATE TABLE IF NOT EXISTS `attendees` (
  `relid` int(11) NOT NULL,
  `accept_status` varchar(3) NOT NULL,
  PRIMARY KEY (`relid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attendees`
--

INSERT INTO `attendees` (`relid`, `accept_status`) VALUES
(3, 'Y'),
(4, 'Y'),
(5, 'U'),
(6, 'Y'),
(7, 'N'),
(8, 'Y'),
(9, 'Y'),
(10, 'N'),
(11, 'Y'),
(12, 'U'),
(13, 'Y'),
(14, 'Y'),
(15, 'Y'),
(16, 'Y'),
(17, 'Y'),
(18, 'Y'),
(19, 'N'),
(20, 'Y'),
(21, 'Y'),
(22, 'Y'),
(23, 'Y'),
(25, 'Y'),
(26, 'Y'),
(27, 'Y'),
(28, 'Y'),
(29, 'Y'),
(30, 'Y'),
(31, 'Y'),
(32, 'Y'),
(34, 'Y'),
(36, 'Y'),
(37, 'U'),
(38, 'Y'),
(39, 'N'),
(48, 'Y'),
(49, 'U'),
(50, 'U'),
(51, 'U');

-- --------------------------------------------------------

--
-- Table structure for table `authtoken_token`
--

CREATE TABLE IF NOT EXISTS `authtoken_token` (
  `key` varchar(40) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`key`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `authtoken_token`
--

INSERT INTO `authtoken_token` (`key`, `user_id`, `created`) VALUES
('0b8eb141e26f028955101545fc501d892adb8cb0', 4, '2013-08-08 20:32:32'),
('313451c8429f9aaf1d7b0d55ef1f16339873f329', 1, '2013-08-05 16:39:13'),
('55f5414c05ed5b829798d9ae4a7f724fd675b283', 3, '2013-08-08 19:42:55'),
('5d91e636c849a7f44d649046d1dec60382f35d1c', 7, '2013-08-08 21:07:14'),
('6fa53f454e1eff35baee04619f6a482dc9e1a61c', 8, '2013-08-08 21:30:28'),
('a36e7bcb49d6ff373271e8597d4df206edd96c9c', 6, '2013-08-08 20:57:50'),
('ca764b487076eb4dc0a2868e37101d5ff248a9cd', 5, '2013-08-08 20:57:11');

-- --------------------------------------------------------

--
-- Table structure for table `auth_group`
--

CREATE TABLE IF NOT EXISTS `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `auth_group_permissions`
--

CREATE TABLE IF NOT EXISTS `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_425ae3c4` (`group_id`),
  KEY `auth_group_permissions_1e014c8f` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `auth_permission`
--

CREATE TABLE IF NOT EXISTS `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  KEY `auth_permission_1bb8f392` (`content_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=88 ;

--
-- Dumping data for table `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add permission', 1, 'add_permission'),
(2, 'Can change permission', 1, 'change_permission'),
(3, 'Can delete permission', 1, 'delete_permission'),
(4, 'Can add group', 2, 'add_group'),
(5, 'Can change group', 2, 'change_group'),
(6, 'Can delete group', 2, 'delete_group'),
(7, 'Can add user', 3, 'add_user'),
(8, 'Can change user', 3, 'change_user'),
(9, 'Can delete user', 3, 'delete_user'),
(10, 'Can add content type', 4, 'add_contenttype'),
(11, 'Can change content type', 4, 'change_contenttype'),
(12, 'Can delete content type', 4, 'delete_contenttype'),
(13, 'Can add session', 5, 'add_session'),
(14, 'Can change session', 5, 'change_session'),
(15, 'Can delete session', 5, 'delete_session'),
(16, 'Can add site', 6, 'add_site'),
(17, 'Can change site', 6, 'change_site'),
(18, 'Can delete site', 6, 'delete_site'),
(19, 'Can add user extended', 7, 'add_userextended'),
(20, 'Can change user extended', 7, 'change_userextended'),
(21, 'Can delete user extended', 7, 'delete_userextended'),
(22, 'Can add what', 8, 'add_what'),
(23, 'Can change what', 8, 'change_what'),
(24, 'Can delete what', 8, 'delete_what'),
(25, 'Can add places', 9, 'add_places'),
(26, 'Can change places', 9, 'change_places'),
(27, 'Can delete places', 9, 'delete_places'),
(28, 'Can add schedules', 10, 'add_schedules'),
(29, 'Can change schedules', 10, 'change_schedules'),
(30, 'Can delete schedules', 10, 'delete_schedules'),
(31, 'Can add events', 11, 'add_events'),
(32, 'Can change events', 11, 'change_events'),
(33, 'Can delete events', 11, 'delete_events'),
(34, 'Can add schedules events', 12, 'add_schedulesevents'),
(35, 'Can change schedules events', 12, 'change_schedulesevents'),
(36, 'Can delete schedules events', 12, 'delete_schedulesevents'),
(37, 'Can add rel attendees', 13, 'add_relattendees'),
(38, 'Can change rel attendees', 13, 'change_relattendees'),
(39, 'Can delete rel attendees', 13, 'delete_relattendees'),
(40, 'Can add attendees', 14, 'add_attendees'),
(41, 'Can change attendees', 14, 'change_attendees'),
(42, 'Can delete attendees', 14, 'delete_attendees'),
(43, 'Can add rel cancellation', 15, 'add_relcancellation'),
(44, 'Can change rel cancellation', 15, 'change_relcancellation'),
(45, 'Can delete rel cancellation', 15, 'delete_relcancellation'),
(46, 'Can add cancellation', 16, 'add_cancellation'),
(47, 'Can change cancellation', 16, 'change_cancellation'),
(48, 'Can delete cancellation', 16, 'delete_cancellation'),
(49, 'Can add rel considers priority', 17, 'add_relconsiderspriority'),
(50, 'Can change rel considers priority', 17, 'change_relconsiderspriority'),
(51, 'Can delete rel considers priority', 17, 'delete_relconsiderspriority'),
(52, 'Can add considers priority', 18, 'add_considerspriority'),
(53, 'Can change considers priority', 18, 'change_considerspriority'),
(54, 'Can delete considers priority', 18, 'delete_considerspriority'),
(55, 'Can add rel schedule attendees', 19, 'add_relscheduleattendees'),
(56, 'Can change rel schedule attendees', 19, 'change_relscheduleattendees'),
(57, 'Can delete rel schedule attendees', 19, 'delete_relscheduleattendees'),
(58, 'Can add schedule attendees', 20, 'add_scheduleattendees'),
(59, 'Can change schedule attendees', 20, 'change_scheduleattendees'),
(60, 'Can delete schedule attendees', 20, 'delete_scheduleattendees'),
(61, 'Can add third party suggestions', 21, 'add_thirdpartysuggestions'),
(62, 'Can change third party suggestions', 21, 'change_thirdpartysuggestions'),
(63, 'Can delete third party suggestions', 21, 'delete_thirdpartysuggestions'),
(64, 'Can add rel third party user', 22, 'add_relthirdpartyuser'),
(65, 'Can change rel third party user', 22, 'change_relthirdpartyuser'),
(66, 'Can delete rel third party user', 22, 'delete_relthirdpartyuser'),
(67, 'Can add third party user', 23, 'add_thirdpartyuser'),
(68, 'Can change third party user', 23, 'change_thirdpartyuser'),
(69, 'Can delete third party user', 23, 'delete_thirdpartyuser'),
(70, 'Can add anon flow', 24, 'add_anonflow'),
(71, 'Can change anon flow', 24, 'change_anonflow'),
(72, 'Can delete anon flow', 24, 'delete_anonflow'),
(73, 'Can add token', 25, 'add_token'),
(74, 'Can change token', 25, 'change_token'),
(75, 'Can delete token', 25, 'delete_token'),
(76, 'Can add account', 26, 'add_account'),
(77, 'Can change account', 26, 'change_account'),
(78, 'Can delete account', 26, 'delete_account'),
(79, 'Can add calendar', 27, 'add_calendar'),
(80, 'Can change calendar', 27, 'change_calendar'),
(81, 'Can delete calendar', 27, 'delete_calendar'),
(82, 'Can add event', 28, 'add_event'),
(83, 'Can change event', 28, 'change_event'),
(84, 'Can delete event', 28, 'delete_event'),
(85, 'Can add log entry', 29, 'add_logentry'),
(86, 'Can change log entry', 29, 'change_logentry'),
(87, 'Can delete log entry', 29, 'delete_logentry');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user`
--

CREATE TABLE IF NOT EXISTS `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(75) NOT NULL,
  `password` varchar(128) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `last_login` datetime NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `auth_user`
--

INSERT INTO `auth_user` (`id`, `username`, `first_name`, `last_name`, `email`, `password`, `is_staff`, `is_active`, `is_superuser`, `last_login`, `date_joined`) VALUES
(1, 'rohitsingh', 'Rohit', 'Singh', 'rohit_s@mit.edu', 'pbkdf2_sha256$10000$SGM4EYxPmbAa$EoCdrs2m7ULAdQ5967+pKG5p1w5pfrDBCaOZmOe06xw=', 1, 1, 1, '2013-08-10 17:26:58', '2013-08-02 21:29:41'),
(3, 'amanda', 'Amanda', 'von Goetz', 'amanda.vongoetz@gmail.com', 'pbkdf2_sha256$10000$1pVEfhmwzpVr$Scmb7wUBlxjVMilYwub6BRMU9IfWcwYXqc34dKpFxyM=', 1, 1, 1, '2013-08-08 19:42:55', '2013-08-08 19:42:55'),
(4, 'daipan', 'Daipan', 'Lee', 'daipanl@mit.edu', 'pbkdf2_sha256$10000$V1oeRcytbmsQ$bPUpi6ocuqV3meP/gVudQ0e9hc3ygKWs8wlnjks/hEI=', 1, 1, 1, '2013-08-08 21:05:01', '2013-08-08 20:32:32'),
(5, 'Andrew', 'Andrew', 'Radin', 'aradin@mit.edu', 'pbkdf2_sha256$10000$XvFwkihW5llX$xJRMsJ54S7t2CUjUsugKbgC6OsjLB1FOP04dUkg5Myk=', 1, 1, 1, '2013-08-08 20:57:11', '2013-08-08 20:57:11'),
(6, 'Meng-Ting', 'Meng-Ting', 'Kao', 'mkao@risd.edu', 'pbkdf2_sha256$10000$e0ul6Dl1je5G$nYllQwP63QygE+3JbdkijsyUy2FLalGKgjv3H0cwhzU=', 0, 1, 0, '2013-08-08 20:57:50', '2013-08-08 20:57:50'),
(7, 'atanas', 'Atanas', 'Baldzhiyski', 'atanasb@mit.edu', 'pbkdf2_sha256$10000$wvAFZom5IVsX$wioK93v/P5YLKJkMEvP5ubEYiXxT0WH6ynWMYePA0SM=', 1, 1, 1, '2013-08-08 21:07:14', '2013-08-08 21:07:14'),
(8, 'rad', 'Andrew', 'Radin', 'rad@mit.edu', 'pbkdf2_sha256$10000$xntKhnWTB4oC$JaiXTZwA09xjZ5f6VunEV4zdYKw73Fhk9xuj4q3VAgg=', 0, 1, 1, '2013-08-08 21:30:28', '2013-08-08 21:30:28');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_groups`
--

CREATE TABLE IF NOT EXISTS `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `auth_user_groups_403f60f` (`user_id`),
  KEY `auth_user_groups_425ae3c4` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_user_permissions`
--

CREATE TABLE IF NOT EXISTS `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `auth_user_user_permissions_403f60f` (`user_id`),
  KEY `auth_user_user_permissions_1e014c8f` (`permission_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=523 ;

--
-- Dumping data for table `auth_user_user_permissions`
--

INSERT INTO `auth_user_user_permissions` (`id`, `user_id`, `permission_id`) VALUES
(88, 3, 1),
(89, 3, 2),
(90, 3, 3),
(91, 3, 4),
(92, 3, 5),
(93, 3, 6),
(94, 3, 7),
(95, 3, 8),
(96, 3, 9),
(97, 3, 10),
(98, 3, 11),
(99, 3, 12),
(100, 3, 13),
(101, 3, 14),
(102, 3, 15),
(103, 3, 16),
(104, 3, 17),
(105, 3, 18),
(106, 3, 19),
(107, 3, 20),
(108, 3, 21),
(109, 3, 22),
(110, 3, 23),
(111, 3, 24),
(112, 3, 25),
(113, 3, 26),
(114, 3, 27),
(115, 3, 28),
(116, 3, 29),
(117, 3, 30),
(118, 3, 31),
(119, 3, 32),
(120, 3, 33),
(121, 3, 34),
(122, 3, 35),
(123, 3, 36),
(124, 3, 37),
(125, 3, 38),
(126, 3, 39),
(127, 3, 40),
(128, 3, 41),
(129, 3, 42),
(130, 3, 43),
(131, 3, 44),
(132, 3, 45),
(133, 3, 46),
(134, 3, 47),
(135, 3, 48),
(136, 3, 49),
(137, 3, 50),
(138, 3, 51),
(139, 3, 52),
(140, 3, 53),
(141, 3, 54),
(142, 3, 55),
(143, 3, 56),
(144, 3, 57),
(145, 3, 58),
(146, 3, 59),
(147, 3, 60),
(148, 3, 61),
(149, 3, 62),
(150, 3, 63),
(151, 3, 64),
(152, 3, 65),
(153, 3, 66),
(154, 3, 67),
(155, 3, 68),
(156, 3, 69),
(157, 3, 70),
(158, 3, 71),
(159, 3, 72),
(160, 3, 73),
(161, 3, 74),
(162, 3, 75),
(163, 3, 76),
(164, 3, 77),
(165, 3, 78),
(166, 3, 79),
(167, 3, 80),
(168, 3, 81),
(169, 3, 82),
(170, 3, 83),
(171, 3, 84),
(172, 3, 85),
(173, 3, 86),
(174, 3, 87),
(175, 4, 1),
(176, 4, 2),
(177, 4, 3),
(178, 4, 4),
(179, 4, 5),
(180, 4, 6),
(181, 4, 7),
(182, 4, 8),
(183, 4, 9),
(184, 4, 10),
(185, 4, 11),
(186, 4, 12),
(187, 4, 13),
(188, 4, 14),
(189, 4, 15),
(190, 4, 16),
(191, 4, 17),
(192, 4, 18),
(193, 4, 19),
(194, 4, 20),
(195, 4, 21),
(196, 4, 22),
(197, 4, 23),
(198, 4, 24),
(199, 4, 25),
(200, 4, 26),
(201, 4, 27),
(202, 4, 28),
(203, 4, 29),
(204, 4, 30),
(205, 4, 31),
(206, 4, 32),
(207, 4, 33),
(208, 4, 34),
(209, 4, 35),
(210, 4, 36),
(211, 4, 37),
(212, 4, 38),
(213, 4, 39),
(214, 4, 40),
(215, 4, 41),
(216, 4, 42),
(217, 4, 43),
(218, 4, 44),
(219, 4, 45),
(220, 4, 46),
(221, 4, 47),
(222, 4, 48),
(223, 4, 49),
(224, 4, 50),
(225, 4, 51),
(226, 4, 52),
(227, 4, 53),
(228, 4, 54),
(229, 4, 55),
(230, 4, 56),
(231, 4, 57),
(232, 4, 58),
(233, 4, 59),
(234, 4, 60),
(235, 4, 61),
(236, 4, 62),
(237, 4, 63),
(238, 4, 64),
(239, 4, 65),
(240, 4, 66),
(241, 4, 67),
(242, 4, 68),
(243, 4, 69),
(244, 4, 70),
(245, 4, 71),
(246, 4, 72),
(247, 4, 73),
(248, 4, 74),
(249, 4, 75),
(250, 4, 76),
(251, 4, 77),
(252, 4, 78),
(253, 4, 79),
(254, 4, 80),
(255, 4, 81),
(256, 4, 82),
(257, 4, 83),
(258, 4, 84),
(259, 4, 85),
(260, 4, 86),
(261, 4, 87),
(436, 7, 1),
(437, 7, 2),
(438, 7, 3),
(439, 7, 4),
(440, 7, 5),
(441, 7, 6),
(442, 7, 7),
(443, 7, 8),
(444, 7, 9),
(445, 7, 10),
(446, 7, 11),
(447, 7, 12),
(448, 7, 13),
(449, 7, 14),
(450, 7, 15),
(451, 7, 16),
(452, 7, 17),
(453, 7, 18),
(454, 7, 19),
(455, 7, 20),
(456, 7, 21),
(457, 7, 22),
(458, 7, 23),
(459, 7, 24),
(460, 7, 25),
(461, 7, 26),
(462, 7, 27),
(463, 7, 28),
(464, 7, 29),
(465, 7, 30),
(466, 7, 31),
(467, 7, 32),
(468, 7, 33),
(469, 7, 34),
(470, 7, 35),
(471, 7, 36),
(472, 7, 37),
(473, 7, 38),
(474, 7, 39),
(475, 7, 40),
(476, 7, 41),
(477, 7, 42),
(478, 7, 43),
(479, 7, 44),
(480, 7, 45),
(481, 7, 46),
(482, 7, 47),
(483, 7, 48),
(484, 7, 49),
(485, 7, 50),
(486, 7, 51),
(487, 7, 52),
(488, 7, 53),
(489, 7, 54),
(490, 7, 55),
(491, 7, 56),
(492, 7, 57),
(493, 7, 58),
(494, 7, 59),
(495, 7, 60),
(496, 7, 61),
(497, 7, 62),
(498, 7, 63),
(499, 7, 64),
(500, 7, 65),
(501, 7, 66),
(502, 7, 67),
(503, 7, 68),
(504, 7, 69),
(505, 7, 70),
(506, 7, 71),
(507, 7, 72),
(508, 7, 73),
(509, 7, 74),
(510, 7, 75),
(511, 7, 76),
(512, 7, 77),
(513, 7, 78),
(514, 7, 79),
(515, 7, 80),
(516, 7, 81),
(517, 7, 82),
(518, 7, 83),
(519, 7, 84),
(520, 7, 85),
(521, 7, 86),
(522, 7, 87);

-- --------------------------------------------------------

--
-- Table structure for table `cancellation`
--

CREATE TABLE IF NOT EXISTS `cancellation` (
  `relid` int(11) NOT NULL,
  `when` datetime NOT NULL,
  `msg` longtext,
  PRIMARY KEY (`relid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `considers_priority`
--

CREATE TABLE IF NOT EXISTS `considers_priority` (
  `relid` int(11) NOT NULL,
  `pr` int(11) NOT NULL,
  PRIMARY KEY (`relid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `django_admin_log`
--

CREATE TABLE IF NOT EXISTS `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_403f60f` (`user_id`),
  KEY `django_admin_log_1bb8f392` (`content_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=152 ;

--
-- Dumping data for table `django_admin_log`
--

INSERT INTO `django_admin_log` (`id`, `action_time`, `user_id`, `content_type_id`, `object_id`, `object_repr`, `action_flag`, `change_message`) VALUES
(1, '2013-08-08 19:42:55', 1, 3, '3', 'amanda', 1, ''),
(2, '2013-08-08 19:43:32', 1, 3, '3', 'amanda', 2, 'Changed password, first_name, last_name, email, is_staff, is_superuser and user_permissions.'),
(3, '2013-08-08 19:43:35', 1, 3, '3', 'amanda', 2, 'Changed password.'),
(4, '2013-08-08 19:44:49', 1, 7, '3', 'UserExtended object', 1, ''),
(5, '2013-08-08 20:20:31', 1, 9, '1', '1:', 3, ''),
(6, '2013-08-08 20:24:43', 1, 9, '2', '2:Martin Trust Center', 1, ''),
(7, '2013-08-08 20:26:00', 1, 11, '2', '2:Dinner with Rohit', 1, ''),
(8, '2013-08-08 20:27:53', 1, 13, '2', '2:Dinner with Rohit,rohitsingh', 1, ''),
(9, '2013-08-08 20:32:32', 1, 3, '4', 'daipan', 1, ''),
(10, '2013-08-08 20:33:01', 1, 3, '4', 'daipan', 2, 'Changed password, first_name, last_name and email.'),
(11, '2013-08-08 20:33:23', 1, 7, '4', 'daipan', 1, ''),
(12, '2013-08-08 20:35:12', 1, 10, '1', '1:2013-08-08 17:35:07-04:00-2013-08-08 19:35:08-04:00', 2, 'Changed begin and end.'),
(13, '2013-08-08 20:35:32', 1, 10, '1', '1:2013-08-10 17:35:07-04:00-2013-08-10 19:35:08-04:00', 2, 'Changed begin and end.'),
(14, '2013-08-08 20:36:53', 1, 10, '2', '2:2013-08-10 20:00:00-04:00-2013-08-10 21:00:00-04:00', 1, ''),
(15, '2013-08-08 20:37:08', 1, 9, '3', '3:E51', 1, ''),
(16, '2013-08-08 20:37:20', 1, 11, '3', '3:Dinner with Rohit', 1, ''),
(17, '2013-08-08 20:38:12', 1, 13, '3', 'rohitsingh@3:Dinner with Rohit', 1, ''),
(18, '2013-08-08 20:38:28', 1, 14, '3', 'rohitsingh@3:Dinner with Rohit->Y', 1, ''),
(19, '2013-08-08 20:55:58', 1, 10, '3', '3:2013-08-09 12:00:00-04:00-2013-08-09 13:30:00-04:00', 1, ''),
(20, '2013-08-08 20:56:08', 1, 9, '4', '4:100 Mem', 1, ''),
(21, '2013-08-08 20:56:13', 1, 11, '4', '4:Ramen with the team', 1, ''),
(22, '2013-08-08 20:57:11', 1, 3, '5', 'Andrew', 1, ''),
(23, '2013-08-08 20:57:26', 1, 3, '5', 'Andrew', 2, 'Changed password, first_name, last_name and email.'),
(24, '2013-08-08 20:57:50', 1, 3, '6', 'Meng-Ting', 1, ''),
(25, '2013-08-08 20:58:16', 1, 3, '6', 'Meng-Ting', 2, 'Changed password, first_name, last_name and email.'),
(26, '2013-08-08 20:58:32', 1, 3, '5', 'Andrew', 2, 'Changed password.'),
(27, '2013-08-08 20:59:11', 1, 7, '5', 'Andrew', 1, ''),
(28, '2013-08-08 21:00:12', 1, 7, '6', 'Meng-Ting', 1, ''),
(29, '2013-08-08 21:01:32', 1, 11, '4', '4:daipan''s event:Ramen with the team', 2, 'No fields changed.'),
(30, '2013-08-08 21:01:53', 1, 13, '4', 'daipan@daipan''s event:4/Ramen with the team', 1, ''),
(31, '2013-08-08 21:02:12', 1, 13, '5', 'rohitsingh@daipan''s event:4/Ramen with the team', 1, ''),
(32, '2013-08-08 21:02:30', 1, 13, '6', 'amanda@daipan''s event:4/Ramen with the team', 1, ''),
(33, '2013-08-08 21:03:23', 1, 13, '7', 'Andrew@daipan''s event:4/Ramen with the team', 1, ''),
(34, '2013-08-08 21:03:33', 1, 13, '8', 'Meng-Ting@daipan''s event:4/Ramen with the team', 1, ''),
(35, '2013-08-08 21:03:44', 1, 3, '4', 'daipan', 2, 'Changed password, is_staff, is_superuser and user_permissions.'),
(36, '2013-08-08 21:07:14', 1, 3, '7', 'atanas', 1, ''),
(37, '2013-08-08 21:07:32', 4, 14, '8', 'Meng-Ting@daipan''s event:4/Ramen with the team->Y', 1, ''),
(38, '2013-08-08 21:07:48', 4, 14, '7', 'Andrew@daipan''s event:4/Ramen with the team->N', 1, ''),
(39, '2013-08-08 21:08:06', 1, 7, '7', 'atanas', 1, ''),
(40, '2013-08-08 21:08:10', 4, 14, '6', 'amanda@daipan''s event:4/Ramen with the team->Y', 1, ''),
(41, '2013-08-08 21:08:21', 4, 14, '5', 'rohitsingh@daipan''s event:4/Ramen with the team->U', 1, ''),
(42, '2013-08-08 21:08:30', 4, 14, '4', 'daipan@daipan''s event:4/Ramen with the team->Y', 1, ''),
(43, '2013-08-08 21:09:12', 1, 3, '7', 'atanas', 2, 'Changed password, first_name, last_name, email and user_permissions.'),
(44, '2013-08-08 21:09:30', 1, 3, '7', 'atanas', 2, 'Changed password and is_superuser.'),
(45, '2013-08-08 21:09:44', 1, 3, '7', 'atanas', 2, 'Changed password and is_staff.'),
(46, '2013-08-08 21:09:44', 4, 10, '4', '4:2014-04-12 20:00:00-04:00-2014-04-12 23:00:00-04:00', 1, ''),
(47, '2013-08-08 21:10:04', 4, 9, '5', '5:The moon, Jupiter, Mars', 1, ''),
(48, '2013-08-08 21:10:09', 4, 11, '5', '5:daipan''s event:Play among the stars', 1, ''),
(49, '2013-08-08 21:10:31', 4, 13, '9', 'amanda@daipan''s event:5/Play among the stars', 1, ''),
(50, '2013-08-08 21:10:40', 4, 13, '10', 'atanas@daipan''s event:5/Play among the stars', 1, ''),
(51, '2013-08-08 21:10:51', 4, 13, '11', 'atanas@daipan''s event:4/Ramen with the team', 1, ''),
(52, '2013-08-08 21:11:01', 4, 14, '11', 'atanas@daipan''s event:4/Ramen with the team->Y', 1, ''),
(53, '2013-08-08 21:11:13', 4, 14, '10', 'atanas@daipan''s event:5/Play among the stars->N', 1, ''),
(54, '2013-08-08 21:11:21', 1, 10, '5', '5:2013-08-10 22:00:00-04:00-2013-08-10 23:00:00-04:00', 1, ''),
(55, '2013-08-08 21:11:28', 4, 14, '9', 'amanda@daipan''s event:5/Play among the stars->Y', 1, ''),
(56, '2013-08-08 21:11:46', 1, 9, '6', '6:Nantucket''s Jetties Beach', 1, ''),
(57, '2013-08-08 21:12:42', 1, 11, '6', '6:atanas''s event:Drinks by the sea', 1, ''),
(58, '2013-08-08 21:13:46', 4, 10, '6', '6:2013-08-08 18:00:00-04:00-2013-08-08 20:00:00-04:00', 1, ''),
(59, '2013-08-08 21:13:56', 1, 10, '7', '7:2013-08-10 18:00:00-04:00-2013-08-10 19:00:00-04:00', 1, ''),
(60, '2013-08-08 21:14:05', 1, 9, '7', '7:The Times', 1, ''),
(61, '2013-08-08 21:14:07', 4, 9, '8', '8:MIT Docks', 1, ''),
(62, '2013-08-08 21:14:13', 1, 11, '7', '7:atanas''s event:Darts at the Times', 1, ''),
(63, '2013-08-08 21:14:17', 4, 11, '8', '8:daipan''s event:Sailing', 1, ''),
(64, '2013-08-08 21:14:55', 4, 10, '8', '8:2013-08-10 21:00:00-04:00-2013-08-10 23:00:00-04:00', 1, ''),
(65, '2013-08-08 21:14:57', 4, 11, '5', '5:daipan''s event:Play among the stars', 2, 'Changed scheduleid.'),
(66, '2013-08-08 21:15:43', 4, 13, '12', 'Meng-Ting@daipan''s event:8/Sailing', 1, ''),
(67, '2013-08-08 21:15:48', 1, 10, '9', '9:2013-08-11 20:00:00-04:00-2013-08-11 20:15:00-04:00', 1, ''),
(68, '2013-08-08 21:15:52', 4, 14, '12', 'Meng-Ting@daipan''s event:8/Sailing->U', 1, ''),
(69, '2013-08-08 21:16:00', 1, 9, '9', '9:Gillian''s Bowling', 1, ''),
(70, '2013-08-08 21:16:48', 1, 9, '10', '10:Jillian''s ', 1, ''),
(71, '2013-08-08 21:16:53', 1, 11, '9', '9:atanas''s event:Can''t Believe It''s Not Gutter - Bowling at Jillians', 1, ''),
(72, '2013-08-08 21:17:35', 4, 10, '10', '10:2013-08-11 06:00:00-04:00-2013-08-11 07:00:00-04:00', 1, ''),
(73, '2013-08-08 21:18:03', 4, 9, '11', '11:Bed', 1, ''),
(74, '2013-08-08 21:18:07', 1, 10, '11', '11:2013-08-11 09:00:00-04:00-2013-08-11 12:00:00-04:00', 1, ''),
(75, '2013-08-08 21:18:09', 4, 11, '10', '10:daipan''s event:Breaking dawn', 1, ''),
(76, '2013-08-08 21:18:18', 1, 9, '12', '12:To the Cape and Back', 1, ''),
(77, '2013-08-08 21:21:24', 4, 10, '12', '12:2013-08-10 21:00:00-04:00-2013-08-10 23:00:00-04:00', 1, ''),
(78, '2013-08-08 21:21:28', 4, 11, '5', '5:daipan''s event:Play among the stars', 2, 'Changed scheduleid.'),
(79, '2013-08-08 21:22:17', 4, 10, '13', '13:2013-08-10 12:00:00-04:00-2013-08-10 13:30:00-04:00', 1, ''),
(80, '2013-08-08 21:22:20', 4, 11, '4', '4:daipan''s event:Ramen with the team', 2, 'Changed scheduleid.'),
(81, '2013-08-08 21:24:52', 4, 11, '10', '10:daipan''s event:Breaking dawn', 2, 'No fields changed.'),
(82, '2013-08-08 21:25:36', 1, 10, '14', '14:2013-08-11 12:00:00-04:00-2013-08-11 14:00:00-04:00', 1, ''),
(83, '2013-08-08 21:25:53', 1, 11, '11', '11:atanas''s event:Road Trip with the Pals', 1, ''),
(84, '2013-08-08 21:26:32', 4, 10, '15', '15:2013-08-10 17:00:00-04:00-2013-08-10 19:00:00-04:00', 1, ''),
(85, '2013-08-08 21:27:07', 1, 10, '16', '16:2013-08-11 15:00:00-04:00-2013-08-11 17:00:00-04:00', 1, ''),
(86, '2013-08-08 21:27:07', 4, 9, '13', '13:Car drive away', 1, ''),
(87, '2013-08-08 21:27:11', 4, 11, '12', '12:daipan''s event:Party', 1, ''),
(88, '2013-08-08 21:27:24', 1, 9, '14', '14:Mark Cuban''s Miami Mansion', 1, ''),
(89, '2013-08-08 21:27:32', 1, 11, '13', '13:atanas''s event:Swimming with the Sharks', 1, ''),
(90, '2013-08-08 21:27:45', 4, 13, '13', 'Andrew@daipan''s event:12/Party', 1, ''),
(91, '2013-08-08 21:27:55', 4, 14, '13', 'Andrew@daipan''s event:12/Party->Y', 1, ''),
(92, '2013-08-08 21:28:22', 1, 13, '14', 'rohitsingh@atanas''s event:6/Drinks by the sea', 1, ''),
(93, '2013-08-08 21:28:37', 4, 10, '17', '17:2013-08-10 09:00:00-04:00-2013-08-10 10:00:00-04:00', 1, ''),
(94, '2013-08-08 21:28:40', 1, 13, '15', 'amanda@atanas''s event:6/Drinks by the sea', 1, ''),
(95, '2013-08-08 21:28:45', 4, 9, '15', '15:Tiffany''s', 1, ''),
(96, '2013-08-08 21:28:50', 4, 11, '14', '14:daipan''s event:Breakfast', 1, ''),
(97, '2013-08-08 21:29:09', 1, 13, '16', 'daipan@atanas''s event:6/Drinks by the sea', 1, ''),
(98, '2013-08-08 21:29:18', 4, 13, '17', 'amanda@daipan''s event:14/Breakfast', 1, ''),
(99, '2013-08-08 21:29:21', 1, 13, '18', 'Andrew@atanas''s event:6/Drinks by the sea', 1, ''),
(100, '2013-08-08 21:29:27', 4, 13, '19', 'atanas@daipan''s event:14/Breakfast', 1, ''),
(101, '2013-08-08 21:29:34', 1, 13, '20', 'Meng-Ting@atanas''s event:6/Drinks by the sea', 1, ''),
(102, '2013-08-08 21:29:40', 4, 14, '19', 'atanas@daipan''s event:14/Breakfast->N', 1, ''),
(103, '2013-08-08 21:29:44', 1, 13, '21', 'rohitsingh@atanas''s event:7/Darts at the Times', 1, ''),
(104, '2013-08-08 21:29:58', 1, 13, '22', 'daipan@atanas''s event:7/Darts at the Times', 1, ''),
(105, '2013-08-08 21:29:59', 4, 14, '17', 'amanda@daipan''s event:14/Breakfast->Y', 1, ''),
(106, '2013-08-08 21:30:11', 1, 13, '23', 'amanda@atanas''s event:7/Darts at the Times', 1, ''),
(107, '2013-08-08 21:30:28', 1, 3, '8', 'rad', 1, ''),
(108, '2013-08-08 21:30:34', 1, 13, '24', 'amanda@atanas''s event:7/Darts at the Times', 1, ''),
(109, '2013-08-08 21:30:55', 1, 13, '25', 'rohitsingh@atanas''s event:9/Can''t Believe It''s Not Gutter - Bowling at Jillians', 1, ''),
(110, '2013-08-08 21:30:56', 1, 3, '8', 'rad', 2, 'Changed password, first_name, last_name, email and is_superuser.'),
(111, '2013-08-08 21:31:06', 1, 13, '26', 'amanda@atanas''s event:9/Can''t Believe It''s Not Gutter - Bowling at Jillians', 1, ''),
(112, '2013-08-08 21:31:16', 1, 13, '27', 'daipan@atanas''s event:9/Can''t Believe It''s Not Gutter - Bowling at Jillians', 1, ''),
(113, '2013-08-08 21:31:26', 1, 13, '28', 'daipan@atanas''s event:9/Can''t Believe It''s Not Gutter - Bowling at Jillians', 1, ''),
(114, '2013-08-08 21:31:43', 1, 13, '29', 'rohitsingh@atanas''s event:11/Road Trip with the Pals', 1, ''),
(115, '2013-08-08 21:31:55', 1, 13, '30', 'amanda@atanas''s event:11/Road Trip with the Pals', 1, ''),
(116, '2013-08-08 21:32:06', 1, 13, '31', 'daipan@atanas''s event:11/Road Trip with the Pals', 1, ''),
(117, '2013-08-08 21:32:16', 1, 13, '32', 'Andrew@atanas''s event:11/Road Trip with the Pals', 1, ''),
(118, '2013-08-08 21:32:28', 1, 13, '33', 'Andrew@atanas''s event:11/Road Trip with the Pals', 1, ''),
(119, '2013-08-08 21:32:38', 1, 13, '34', 'rohitsingh@atanas''s event:13/Swimming with the Sharks', 1, ''),
(120, '2013-08-08 21:32:47', 1, 13, '35', 'amanda@atanas''s event:13/Swimming with the Sharks', 1, ''),
(121, '2013-08-08 21:32:56', 1, 13, '36', 'amanda@atanas''s event:13/Swimming with the Sharks', 1, ''),
(122, '2013-08-08 21:33:05', 1, 13, '37', 'daipan@atanas''s event:13/Swimming with the Sharks', 1, ''),
(123, '2013-08-08 21:33:16', 1, 13, '38', 'Andrew@atanas''s event:13/Swimming with the Sharks', 1, ''),
(124, '2013-08-08 21:33:25', 1, 13, '39', 'Meng-Ting@atanas''s event:13/Swimming with the Sharks', 1, ''),
(125, '2013-08-08 21:34:08', 1, 14, '14', 'rohitsingh@atanas''s event:6/Drinks by the sea->Y', 1, ''),
(126, '2013-08-08 21:34:24', 1, 14, '15', 'amanda@atanas''s event:6/Drinks by the sea->Y', 1, ''),
(127, '2013-08-08 21:34:40', 1, 14, '16', 'daipan@atanas''s event:6/Drinks by the sea->Y', 1, ''),
(128, '2013-08-08 21:34:53', 1, 3, '5', 'Andrew', 2, 'Changed password, is_staff and is_superuser.'),
(129, '2013-08-08 21:35:00', 1, 14, '18', 'Andrew@atanas''s event:6/Drinks by the sea->Y', 1, ''),
(130, '2013-08-08 21:35:20', 1, 14, '20', 'Meng-Ting@atanas''s event:6/Drinks by the sea->Y', 1, ''),
(131, '2013-08-08 21:35:34', 1, 14, '21', 'rohitsingh@atanas''s event:7/Darts at the Times->Y', 1, ''),
(132, '2013-08-08 21:35:54', 1, 14, '22', 'daipan@atanas''s event:7/Darts at the Times->Y', 1, ''),
(133, '2013-08-08 21:36:13', 1, 14, '23', 'amanda@atanas''s event:7/Darts at the Times->Y', 1, ''),
(134, '2013-08-08 21:36:36', 1, 14, '25', 'rohitsingh@atanas''s event:9/Can''t Believe It''s Not Gutter - Bowling at Jillians->Y', 1, ''),
(135, '2013-08-08 21:36:50', 1, 14, '26', 'amanda@atanas''s event:9/Can''t Believe It''s Not Gutter - Bowling at Jillians->Y', 1, ''),
(136, '2013-08-08 21:37:02', 1, 14, '27', 'daipan@atanas''s event:9/Can''t Believe It''s Not Gutter - Bowling at Jillians->Y', 1, ''),
(137, '2013-08-08 21:48:20', 1, 14, '28', 'daipan@atanas''s event:9/Can''t Believe It''s Not Gutter - Bowling at Jillians->Y', 1, ''),
(138, '2013-08-08 21:48:43', 1, 14, '29', 'rohitsingh@atanas''s event:11/Road Trip with the Pals->Y', 1, ''),
(139, '2013-08-08 21:48:56', 1, 14, '30', 'amanda@atanas''s event:11/Road Trip with the Pals->Y', 1, ''),
(140, '2013-08-08 21:49:08', 1, 14, '31', 'daipan@atanas''s event:11/Road Trip with the Pals->Y', 1, ''),
(141, '2013-08-08 21:49:25', 1, 14, '32', 'Andrew@atanas''s event:11/Road Trip with the Pals->Y', 1, ''),
(142, '2013-08-08 21:49:47', 1, 14, '34', 'rohitsingh@atanas''s event:13/Swimming with the Sharks->Y', 1, ''),
(143, '2013-08-08 21:50:04', 1, 14, '36', 'amanda@atanas''s event:13/Swimming with the Sharks->Y', 1, ''),
(144, '2013-08-08 21:50:19', 1, 14, '37', 'daipan@atanas''s event:13/Swimming with the Sharks->Y', 1, ''),
(145, '2013-08-08 21:50:35', 1, 14, '38', 'Andrew@atanas''s event:13/Swimming with the Sharks->Y', 1, ''),
(146, '2013-08-08 21:50:46', 1, 14, '39', 'Meng-Ting@atanas''s event:13/Swimming with the Sharks->N', 1, ''),
(147, '2013-08-08 21:51:00', 1, 14, '37', 'daipan@atanas''s event:13/Swimming with the Sharks->U', 2, 'Changed accept_status.'),
(148, '2013-08-10 16:13:20', 1, 11, '17', '17:rohitsingh''s event:Team meeting for investment | Saturday 08-10 | 15:00-16:00', 3, ''),
(149, '2013-08-10 16:13:20', 1, 11, '16', '16:rohitsingh''s event:Team meeting for investment | Saturday 08-10 | 15:00-16:00', 3, ''),
(150, '2013-08-10 16:14:07', 1, 11, '18', '18:rohitsingh''s event:Team meeting for investment | Saturday 08-10 | 15:00-16:00', 3, ''),
(151, '2013-08-10 16:14:07', 1, 11, '15', '15:rohitsingh''s event:Team meeting for investment | Saturday 08-10 | 15:00-16:00', 3, '');

-- --------------------------------------------------------

--
-- Table structure for table `django_content_type`
--

CREATE TABLE IF NOT EXISTS `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_label` (`app_label`,`model`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `name`, `app_label`, `model`) VALUES
(1, 'permission', 'auth', 'permission'),
(2, 'group', 'auth', 'group'),
(3, 'user', 'auth', 'user'),
(4, 'content type', 'contenttypes', 'contenttype'),
(5, 'session', 'sessions', 'session'),
(6, 'site', 'sites', 'site'),
(7, 'user extended', 'backend', 'userextended'),
(8, 'what', 'backend', 'what'),
(9, 'places', 'backend', 'places'),
(10, 'schedules', 'backend', 'schedules'),
(11, 'events', 'backend', 'events'),
(12, 'schedules events', 'backend', 'schedulesevents'),
(13, 'rel attendees', 'backend', 'relattendees'),
(14, 'attendees', 'backend', 'attendees'),
(15, 'rel cancellation', 'backend', 'relcancellation'),
(16, 'cancellation', 'backend', 'cancellation'),
(17, 'rel considers priority', 'backend', 'relconsiderspriority'),
(18, 'considers priority', 'backend', 'considerspriority'),
(19, 'rel schedule attendees', 'backend', 'relscheduleattendees'),
(20, 'schedule attendees', 'backend', 'scheduleattendees'),
(21, 'third party suggestions', 'backend', 'thirdpartysuggestions'),
(22, 'rel third party user', 'backend', 'relthirdpartyuser'),
(23, 'third party user', 'backend', 'thirdpartyuser'),
(24, 'anon flow', 'backend', 'anonflow'),
(25, 'token', 'authtoken', 'token'),
(26, 'account', 'google', 'account'),
(27, 'calendar', 'google', 'calendar'),
(28, 'event', 'google', 'event'),
(29, 'log entry', 'admin', 'logentry');

-- --------------------------------------------------------

--
-- Table structure for table `django_session`
--

CREATE TABLE IF NOT EXISTS `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_3da3d3d8` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('01921caae443ab745f7f68bbdef5ddf6', 'MmNmNTRhNjMxZWM5NWEyZGI4NWIxMGY3MzJkZjAxMTU0NWU5MGQzODqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQF1Lg==\n', '2013-08-24 17:26:58'),
('0561a4d172cbff1a49b9193ede43aadc', 'MmNmNTRhNjMxZWM5NWEyZGI4NWIxMGY3MzJkZjAxMTU0NWU5MGQzODqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQF1Lg==\n', '2013-08-20 16:42:51'),
('130b95468fe778df206a79fb5f92853c', 'NmE0MjQ5M2M3ZmRhYjNlYjY5NDA5NjczNDk4YjM0NWU0OGY3YmI4NzqAAn1xAVUKdGVzdGNvb2tp\nZVUGd29ya2VkcQJzLg==\n', '2013-08-22 21:47:20'),
('1fb0da1e11564f5241b863d01405378c', 'MmNmNTRhNjMxZWM5NWEyZGI4NWIxMGY3MzJkZjAxMTU0NWU5MGQzODqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQF1Lg==\n', '2013-08-20 16:39:43'),
('251d0df5a0d56eaf88c9bf91a6427bae', 'MmNmNTRhNjMxZWM5NWEyZGI4NWIxMGY3MzJkZjAxMTU0NWU5MGQzODqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQF1Lg==\n', '2013-08-22 18:59:46'),
('303faddf5bdf7bd78a64d28ae82f8b07', 'MmNmNTRhNjMxZWM5NWEyZGI4NWIxMGY3MzJkZjAxMTU0NWU5MGQzODqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQF1Lg==\n', '2013-08-22 19:41:25'),
('4c108fe3cc318e0bbb066639b1b11657', 'MmNmNTRhNjMxZWM5NWEyZGI4NWIxMGY3MzJkZjAxMTU0NWU5MGQzODqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQF1Lg==\n', '2013-08-20 16:39:54'),
('518290a57cc17e4c64cf81f4974b17ae', 'YjYyZjdhZGQ3Mjc3NTYxZTU0NjE0MThmODc4MDE0ZTI2YTJlOWY0MDqAAn1xAS4=\n', '2013-08-22 21:30:54'),
('561d18796043493c790aa8ffa59ec886', 'NjdmM2EyODE3YmY4MmNjYmFhMDNlMzEzMDYzNDY5NGY1ZGNlMGVlZDqAAn1xAVUKdGVzdGNvb2tp\nZXECVQZ3b3JrZWRxA3Mu\n', '2013-08-24 17:25:00'),
('6d69e00ad5057fbda03459b056e9116c', 'MmNmNTRhNjMxZWM5NWEyZGI4NWIxMGY3MzJkZjAxMTU0NWU5MGQzODqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQF1Lg==\n', '2013-08-23 20:07:12'),
('71cb26ca81baea1d8464c96f213bb9db', 'NmE0MjQ5M2M3ZmRhYjNlYjY5NDA5NjczNDk4YjM0NWU0OGY3YmI4NzqAAn1xAVUKdGVzdGNvb2tp\nZVUGd29ya2VkcQJzLg==\n', '2013-08-22 21:37:01'),
('80258f77a3b3448c3076e62829b31094', 'MmNmNTRhNjMxZWM5NWEyZGI4NWIxMGY3MzJkZjAxMTU0NWU5MGQzODqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQF1Lg==\n', '2013-08-20 16:45:22'),
('adf09a6ffc6fa50c5e9e2fc8d9b46be1', 'MmNmNTRhNjMxZWM5NWEyZGI4NWIxMGY3MzJkZjAxMTU0NWU5MGQzODqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQF1Lg==\n', '2013-08-20 16:42:06'),
('cf67b9cc12585922cc80712821d68a3a', 'MmNmNTRhNjMxZWM5NWEyZGI4NWIxMGY3MzJkZjAxMTU0NWU5MGQzODqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQF1Lg==\n', '2013-08-20 16:39:51'),
('e1bae6108a505da93c274b27f2129925', 'YjYyZjdhZGQ3Mjc3NTYxZTU0NjE0MThmODc4MDE0ZTI2YTJlOWY0MDqAAn1xAS4=\n', '2013-08-22 21:51:09');

-- --------------------------------------------------------

--
-- Table structure for table `django_site`
--

CREATE TABLE IF NOT EXISTS `django_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `django_site`
--

INSERT INTO `django_site` (`id`, `domain`, `name`) VALUES
(1, 'example.com', 'example.com');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE IF NOT EXISTS `events` (
  `eid` int(11) NOT NULL AUTO_INCREMENT,
  `tag` varchar(15) NOT NULL,
  `uid` int(11) NOT NULL,
  `title` varchar(1500) NOT NULL,
  `whatid` int(11) NOT NULL,
  `scheduleid` int(11) NOT NULL,
  `placeid` int(11) NOT NULL,
  `dead` varchar(3) NOT NULL,
  PRIMARY KEY (`eid`),
  KEY `events_51c63bce` (`uid`),
  KEY `events_42b85e24` (`whatid`),
  KEY `events_b8a4909` (`scheduleid`),
  KEY `events_6224e6e6` (`placeid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`eid`, `tag`, `uid`, `title`, `whatid`, `scheduleid`, `placeid`, `dead`) VALUES
(2, 'APP', 3, 'Dinner with Rohit', 5, 1, 2, '-1'),
(3, 'APP', 4, 'Dinner with Rohit', 5, 2, 3, '-1'),
(4, 'APP', 4, 'Ramen with the team', 3, 13, 4, '-1'),
(5, 'APP', 4, 'Play among the stars', 7, 12, 5, '-1'),
(6, 'APP', 7, 'Drinks by the sea', 6, 5, 6, '-1'),
(7, 'GC1', 7, 'Darts at the Times', 1, 7, 7, '-1'),
(8, 'GC1', 4, 'Sailing', 7, 6, 8, '-1'),
(9, 'APP', 7, 'Can''t Believe It''s Not Gutter - Bowling at Jillians', 7, 9, 10, '-1'),
(10, 'APP', 4, 'Breaking dawn', 2, 10, 11, '-1'),
(11, 'APP', 7, 'Road Trip with the Pals', 7, 14, 12, '-1'),
(12, 'GC1', 4, 'Party', 7, 15, 13, '-1'),
(13, 'APP', 7, 'Swimming with the Sharks', 1, 16, 14, '-1'),
(14, 'APP', 4, 'Breakfast', 2, 17, 15, '-1'),
(19, 'APP', 1, 'Team meeting for broadvestment', 3, 27, 17, '-1');

-- --------------------------------------------------------

--
-- Table structure for table `google_account`
--

CREATE TABLE IF NOT EXISTS `google_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `token` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `google_account`
--

INSERT INTO `google_account` (`id`, `email`, `password`, `token`) VALUES
(2, '', '', '1/eo175ueD7maagdlqlM0_rdYoRPc62uuKyU_aG-vVXjI'),
(3, '', '', '1/u31ttXeid3UGIceOCpffvsOBDD2NNiXfD7F8DFCg2Ww');

-- --------------------------------------------------------

--
-- Table structure for table `google_calendar`
--

CREATE TABLE IF NOT EXISTS `google_calendar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `uri` varchar(255) NOT NULL,
  `title` varchar(100) NOT NULL,
  `where` varchar(100) NOT NULL,
  `color` varchar(10) NOT NULL,
  `timezone` varchar(100) NOT NULL,
  `summary` longtext NOT NULL,
  `feed_uri` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uri` (`uri`),
  KEY `google_calendar_6f2fe10e` (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `google_event`
--

CREATE TABLE IF NOT EXISTS `google_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `calendar_id` int(11) NOT NULL,
  `uri` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `edit_uri` varchar(255) NOT NULL,
  `view_uri` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uri` (`uri`),
  KEY `google_event_447205e2` (`calendar_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `places`
--

CREATE TABLE IF NOT EXISTS `places` (
  `placeid` int(11) NOT NULL AUTO_INCREMENT,
  `loc_desc` longtext NOT NULL,
  `longitude` double DEFAULT NULL,
  `lattitude` double DEFAULT NULL,
  `address` longtext,
  PRIMARY KEY (`placeid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `places`
--

INSERT INTO `places` (`placeid`, `loc_desc`, `longitude`, `lattitude`, `address`) VALUES
(2, 'Martin Trust Center', NULL, NULL, ''),
(3, 'E51', NULL, NULL, ''),
(4, '100 Mem', NULL, NULL, ''),
(5, 'The moon, Jupiter, Mars', NULL, NULL, ''),
(6, 'Nantucket''s Jetties Beach', NULL, NULL, ''),
(7, 'The Times', NULL, NULL, ''),
(8, 'MIT Docks', NULL, NULL, ''),
(9, 'Gillian''s Bowling', NULL, NULL, ''),
(10, 'Jillian''s ', NULL, NULL, ''),
(11, 'Bed', NULL, NULL, ''),
(12, 'To the Cape and Back', NULL, NULL, ''),
(13, 'Car drive away', NULL, NULL, ''),
(14, 'Mark Cuban''s Miami Mansion', NULL, NULL, ''),
(15, 'Tiffany''s', NULL, NULL, ''),
(16, 'Stata Center, G7', NULL, NULL, NULL),
(17, 'Stata Center, G9', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `rel_attendees`
--

CREATE TABLE IF NOT EXISTS `rel_attendees` (
  `eid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `relid` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`relid`),
  KEY `rel_attendees_2310b62` (`eid`),
  KEY `rel_attendees_51c63bce` (`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=52 ;

--
-- Dumping data for table `rel_attendees`
--

INSERT INTO `rel_attendees` (`eid`, `uid`, `relid`) VALUES
(2, 1, 2),
(3, 1, 3),
(4, 4, 4),
(4, 1, 5),
(4, 3, 6),
(4, 5, 7),
(4, 6, 8),
(5, 3, 9),
(5, 7, 10),
(4, 7, 11),
(8, 6, 12),
(12, 5, 13),
(6, 1, 14),
(6, 3, 15),
(6, 4, 16),
(14, 3, 17),
(6, 5, 18),
(14, 7, 19),
(6, 6, 20),
(7, 1, 21),
(7, 4, 22),
(7, 3, 23),
(7, 3, 24),
(9, 1, 25),
(9, 3, 26),
(9, 4, 27),
(9, 4, 28),
(11, 1, 29),
(11, 3, 30),
(11, 4, 31),
(11, 5, 32),
(11, 5, 33),
(13, 1, 34),
(13, 3, 35),
(13, 3, 36),
(13, 4, 37),
(13, 5, 38),
(13, 6, 39),
(19, 1, 48),
(19, 4, 49),
(19, 3, 50),
(19, 5, 51);

-- --------------------------------------------------------

--
-- Table structure for table `rel_cancellation`
--

CREATE TABLE IF NOT EXISTS `rel_cancellation` (
  `eid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `relid` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`relid`),
  KEY `rel_cancellation_2310b62` (`eid`),
  KEY `rel_cancellation_51c63bce` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `rel_considers_priority`
--

CREATE TABLE IF NOT EXISTS `rel_considers_priority` (
  `uid_l` int(11) NOT NULL,
  `uid_r` int(11) NOT NULL,
  `relid` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`relid`),
  KEY `rel_considers_priority_2fd64ce3` (`uid_l`),
  KEY `rel_considers_priority_30abec8d` (`uid_r`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `rel_schedule_attendees`
--

CREATE TABLE IF NOT EXISTS `rel_schedule_attendees` (
  `scheduleid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `relid` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`relid`),
  KEY `rel_schedule_attendees_b8a4909` (`scheduleid`),
  KEY `rel_schedule_attendees_51c63bce` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `rel_third_party_user`
--

CREATE TABLE IF NOT EXISTS `rel_third_party_user` (
  `whatid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `tpid` int(11) NOT NULL,
  `relid` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`relid`),
  KEY `rel_third_party_user_42b85e24` (`whatid`),
  KEY `rel_third_party_user_51c63bce` (`uid`),
  KEY `rel_third_party_user_1ae027e8` (`tpid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `schedules`
--

CREATE TABLE IF NOT EXISTS `schedules` (
  `scheduleid` int(11) NOT NULL AUTO_INCREMENT,
  `begin` datetime NOT NULL,
  `end` datetime NOT NULL,
  PRIMARY KEY (`scheduleid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `schedules`
--

INSERT INTO `schedules` (`scheduleid`, `begin`, `end`) VALUES
(1, '2013-08-10 21:35:07', '2013-08-10 23:35:08'),
(2, '2013-08-11 00:00:00', '2013-08-11 01:00:00'),
(3, '2013-08-09 16:00:00', '2013-08-09 17:30:00'),
(4, '2014-04-13 00:00:00', '2014-04-13 03:00:00'),
(5, '2013-08-11 02:00:00', '2013-08-11 03:00:00'),
(6, '2013-08-08 22:00:00', '2013-08-09 00:00:00'),
(7, '2013-08-10 22:00:00', '2013-08-10 23:00:00'),
(8, '2013-08-11 01:00:00', '2013-08-11 03:00:00'),
(9, '2013-08-12 00:00:00', '2013-08-12 00:15:00'),
(10, '2013-08-11 10:00:00', '2013-08-11 11:00:00'),
(11, '2013-08-11 13:00:00', '2013-08-11 16:00:00'),
(12, '2013-08-11 01:00:00', '2013-08-11 03:00:00'),
(13, '2013-08-10 16:00:00', '2013-08-10 17:30:00'),
(14, '2013-08-11 16:00:00', '2013-08-11 18:00:00'),
(15, '2013-08-10 21:00:00', '2013-08-10 23:00:00'),
(16, '2013-08-11 19:00:00', '2013-08-11 21:00:00'),
(17, '2013-08-10 13:00:00', '2013-08-10 14:00:00'),
(18, '2013-08-10 15:00:00', '2013-08-10 16:00:00'),
(19, '2013-08-10 15:00:00', '2013-08-10 16:00:00'),
(20, '2013-08-10 15:00:00', '2013-08-10 16:00:00'),
(21, '2013-08-10 15:00:00', '2013-08-10 16:00:00'),
(22, '2013-08-10 15:00:00', '2013-08-10 16:00:00'),
(23, '2013-08-10 15:00:00', '2013-08-10 16:00:00'),
(24, '2013-08-10 17:00:00', '2013-08-10 18:00:00'),
(25, '2013-08-10 17:00:00', '2013-08-10 18:00:00'),
(26, '2013-08-10 17:00:00', '2013-08-10 18:00:00'),
(27, '2013-08-10 17:00:00', '2013-08-10 18:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `schedules_events`
--

CREATE TABLE IF NOT EXISTS `schedules_events` (
  `scheduleid` int(11) NOT NULL,
  `eid` int(11) NOT NULL,
  PRIMARY KEY (`scheduleid`),
  KEY `schedules_events_2310b62` (`eid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `schedule_attendees`
--

CREATE TABLE IF NOT EXISTS `schedule_attendees` (
  `relid` int(11) NOT NULL,
  `status` varchar(3) NOT NULL,
  PRIMARY KEY (`relid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `third_party_suggestions`
--

CREATE TABLE IF NOT EXISTS `third_party_suggestions` (
  `tpid` int(11) NOT NULL AUTO_INCREMENT,
  `placeid` int(11) NOT NULL,
  `link` varchar(1500) NOT NULL,
  `info` longtext,
  PRIMARY KEY (`tpid`),
  KEY `third_party_suggestions_6224e6e6` (`placeid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `third_party_user`
--

CREATE TABLE IF NOT EXISTS `third_party_user` (
  `status` varchar(3) NOT NULL,
  `when` datetime NOT NULL,
  `relid` int(11) NOT NULL,
  PRIMARY KEY (`relid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_extended`
--

CREATE TABLE IF NOT EXISTS `user_extended` (
  `uid` int(11) NOT NULL,
  `phone` varchar(60) DEFAULT NULL,
  `fbid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_extended`
--

INSERT INTO `user_extended` (`uid`, `phone`, `fbid`) VALUES
(1, '6172338505', 'rohitsingh0812'),
(3, '6464967662', 'avongoetz'),
(4, '6175018154', ''),
(5, '6178211151', ''),
(6, '6262155041', ''),
(7, '508-292-0172', '');

-- --------------------------------------------------------

--
-- Table structure for table `what`
--

CREATE TABLE IF NOT EXISTS `what` (
  `whatid` int(11) NOT NULL AUTO_INCREMENT,
  `activity` varchar(150) DEFAULT NULL,
  `parentid` int(11) NOT NULL,
  PRIMARY KEY (`whatid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `what`
--

INSERT INTO `what` (`whatid`, `activity`, `parentid`) VALUES
(1, 'Work Meeting', -1),
(2, 'Breakfast', -1),
(3, 'Lunch', -1),
(4, 'Coffee', -1),
(5, 'Dinner', -1),
(6, 'Drinks', -1),
(7, 'Other', -1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `anon_flow`
--
ALTER TABLE `anon_flow`
  ADD CONSTRAINT `uid_refs_uid_38c1589b` FOREIGN KEY (`uid`) REFERENCES `user_extended` (`uid`);

--
-- Constraints for table `attendees`
--
ALTER TABLE `attendees`
  ADD CONSTRAINT `relid_refs_relid_12b835a1` FOREIGN KEY (`relid`) REFERENCES `rel_attendees` (`relid`);

--
-- Constraints for table `authtoken_token`
--
ALTER TABLE `authtoken_token`
  ADD CONSTRAINT `user_id_refs_id_5656ace4` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `group_id_refs_id_3cea63fe` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `permission_id_refs_id_5886d21f` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`);

--
-- Constraints for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `content_type_id_refs_id_728de91f` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Constraints for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD CONSTRAINT `group_id_refs_id_f116770` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `user_id_refs_id_7ceef80f` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD CONSTRAINT `permission_id_refs_id_67e79cb` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `user_id_refs_id_dfbab7d` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `cancellation`
--
ALTER TABLE `cancellation`
  ADD CONSTRAINT `relid_refs_relid_6ca90fb1` FOREIGN KEY (`relid`) REFERENCES `rel_cancellation` (`relid`);

--
-- Constraints for table `considers_priority`
--
ALTER TABLE `considers_priority`
  ADD CONSTRAINT `relid_refs_relid_7b924e87` FOREIGN KEY (`relid`) REFERENCES `rel_considers_priority` (`relid`);

--
-- Constraints for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `content_type_id_refs_id_288599e6` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `user_id_refs_id_c8665aa` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `events`
--
ALTER TABLE `events`
  ADD CONSTRAINT `placeid_refs_placeid_6f85fce0` FOREIGN KEY (`placeid`) REFERENCES `places` (`placeid`),
  ADD CONSTRAINT `scheduleid_refs_scheduleid_747f6173` FOREIGN KEY (`scheduleid`) REFERENCES `schedules` (`scheduleid`),
  ADD CONSTRAINT `uid_refs_uid_fd4108c` FOREIGN KEY (`uid`) REFERENCES `user_extended` (`uid`),
  ADD CONSTRAINT `whatid_refs_whatid_2c52b294` FOREIGN KEY (`whatid`) REFERENCES `what` (`whatid`);

--
-- Constraints for table `google_calendar`
--
ALTER TABLE `google_calendar`
  ADD CONSTRAINT `account_id_refs_id_47719f91` FOREIGN KEY (`account_id`) REFERENCES `google_account` (`id`);

--
-- Constraints for table `google_event`
--
ALTER TABLE `google_event`
  ADD CONSTRAINT `calendar_id_refs_id_29549090` FOREIGN KEY (`calendar_id`) REFERENCES `google_calendar` (`id`);

--
-- Constraints for table `rel_attendees`
--
ALTER TABLE `rel_attendees`
  ADD CONSTRAINT `eid_refs_eid_1e008dfc` FOREIGN KEY (`eid`) REFERENCES `events` (`eid`),
  ADD CONSTRAINT `uid_refs_uid_470cc41d` FOREIGN KEY (`uid`) REFERENCES `user_extended` (`uid`);

--
-- Constraints for table `rel_cancellation`
--
ALTER TABLE `rel_cancellation`
  ADD CONSTRAINT `eid_refs_eid_38fd1f19` FOREIGN KEY (`eid`) REFERENCES `events` (`eid`),
  ADD CONSTRAINT `uid_refs_uid_282c5300` FOREIGN KEY (`uid`) REFERENCES `user_extended` (`uid`);

--
-- Constraints for table `rel_considers_priority`
--
ALTER TABLE `rel_considers_priority`
  ADD CONSTRAINT `uid_l_refs_uid_127fc768` FOREIGN KEY (`uid_l`) REFERENCES `user_extended` (`uid`),
  ADD CONSTRAINT `uid_r_refs_uid_127fc768` FOREIGN KEY (`uid_r`) REFERENCES `user_extended` (`uid`);

--
-- Constraints for table `rel_schedule_attendees`
--
ALTER TABLE `rel_schedule_attendees`
  ADD CONSTRAINT `scheduleid_refs_scheduleid_589f51c3` FOREIGN KEY (`scheduleid`) REFERENCES `schedules` (`scheduleid`),
  ADD CONSTRAINT `uid_refs_uid_5e0d5c9c` FOREIGN KEY (`uid`) REFERENCES `user_extended` (`uid`);

--
-- Constraints for table `rel_third_party_user`
--
ALTER TABLE `rel_third_party_user`
  ADD CONSTRAINT `tpid_refs_tpid_352c936` FOREIGN KEY (`tpid`) REFERENCES `third_party_suggestions` (`tpid`),
  ADD CONSTRAINT `uid_refs_uid_aa0ffc7` FOREIGN KEY (`uid`) REFERENCES `user_extended` (`uid`),
  ADD CONSTRAINT `whatid_refs_whatid_72d9c359` FOREIGN KEY (`whatid`) REFERENCES `what` (`whatid`);

--
-- Constraints for table `schedules_events`
--
ALTER TABLE `schedules_events`
  ADD CONSTRAINT `eid_refs_eid_361d32ee` FOREIGN KEY (`eid`) REFERENCES `events` (`eid`),
  ADD CONSTRAINT `scheduleid_refs_scheduleid_649eb454` FOREIGN KEY (`scheduleid`) REFERENCES `schedules` (`scheduleid`);

--
-- Constraints for table `schedule_attendees`
--
ALTER TABLE `schedule_attendees`
  ADD CONSTRAINT `relid_refs_relid_18ad1c1f` FOREIGN KEY (`relid`) REFERENCES `rel_schedule_attendees` (`relid`);

--
-- Constraints for table `third_party_suggestions`
--
ALTER TABLE `third_party_suggestions`
  ADD CONSTRAINT `placeid_refs_placeid_6c8e0c6e` FOREIGN KEY (`placeid`) REFERENCES `places` (`placeid`);

--
-- Constraints for table `third_party_user`
--
ALTER TABLE `third_party_user`
  ADD CONSTRAINT `relid_refs_relid_467e0df7` FOREIGN KEY (`relid`) REFERENCES `rel_third_party_user` (`relid`);

--
-- Constraints for table `user_extended`
--
ALTER TABLE `user_extended`
  ADD CONSTRAINT `uid_refs_id_4009ba16` FOREIGN KEY (`uid`) REFERENCES `auth_user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
