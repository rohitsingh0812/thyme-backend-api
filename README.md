# README #

### LIcense ####
    Copyright 2014, Thyme Labs Inc.

    "Thyme Backend API" is free software: you can redistribute it and/or modify 
    it under the terms of the GNU General Public License as published by the Free 
    Software Foundation, either version 3 of the License, or (at your option) 
    any later version.

    "Thyme Backend API" is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License 
    along with "Thyme Backend API".  If not, see<http://www.gnu.org/licenses/>.

### What is this repository for? ###

*Thyme Backend API provides a Django REST interface to find time to meet with your friends. The repository also contains a web based authentication mechanism for GCal and ability to add calendar data through API calls as well. Once the server has calendar data for a few accounts - it can quickly find the best possible times to meet.

### How do I get set up? ###

A quick way of setting up would be to install  bitnami django stack: http://wiki.bitnami.com/Infrastructure_Stacks/BitNami_Django_Stack 

And then setting up this repository as a Django REST framework based server: http://www.django-rest-framework.org/#installation 

### Who do I talk to? ###

* Repo owner: rohitsingh0812